# blog/urls.py
from django.conf.urls import url
from django.urls import path

from f_1_2_sites import views

urlpatterns = [
  url(r'^$', views.SiteListView.as_view(), name='site_list'),
  
  path('test-import', views.test_import, name='test'),
  path('test-get-all-site', views.test_get_all_site, name='test_get_all_site'),
  
  path('site-map', views.SiteMap.as_view(), name='site_map'),  

  path('mpd-map', views.MPDMap.as_view(), name='mpd_map'),  



]
  