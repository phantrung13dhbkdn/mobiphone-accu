# Generated by Django 3.2.8 on 2022-09-07 17:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('f_1_2_sites', '0002_auto_20220617_0001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='site',
            name='id',
            field=models.CharField(max_length=100, primary_key=True, serialize=False, unique=True),
        ),
    ]
