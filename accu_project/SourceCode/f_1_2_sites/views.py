from csv import list_dialects
from __Config.Include.Common_Include import *
from f_1_2_sites.models import Site
from django.contrib.auth import get_user_model
import os
import pandas
from accu_dashboard.models import  ACCUData, MPDInfo




class SiteListView(PermissionRequiredMixin, ListView):
  model = Site
  permission_required = ['f_1_2_sites.view_site']
  template_name = 'apps/f_1_2_sites/site_list.html'
  context_object_name = 'list_site'
  ordering = ['name']

  def get_queryset(self):
    #1)Dữ liệu
    qs = super().get_queryset()
    
    # #1.2)Dữ liệu từ request
    #3)Trả về
    return qs
  
  def get_context_data(self, **kwargs):
    #1)Dữ liệu
    data = super().get_context_data()
    return data


def test_import(request):

  data_file = "/Users/phantrung/dev/MOBIPHONE-new/data/site_infor_thieu.csv"
  df = pandas.read_csv(data_file)
  list_site_id = []
  list_ptm_code = []
  list_provice = []
  list_district = []
  list_address = []
  list_long = []
  list_lat = []
  list_group = []
  list_village = []


  list_key = ['Site_ID', 'Mã PTM', 'Tỉnh/TP', 'Quận/Huyện', 'ĐỊA CHỈ',
       'Long (Kinh độ)', 'Lat (Vĩ độ)', 'Đài VT', 'Phường/Xã']

  for i in df['Site_ID']:
    list_site_id.append(i)
  
  for i in df['Mã PTM']:
    list_ptm_code.append(i)

  for i in df['Tỉnh/TP']:
    list_provice.append(i)

  for i in df['Quận/Huyện']:
    list_district.append(i)

  for i in df['ĐỊA CHỈ']:
    list_address.append(i)
  
  for i in df['Long (Kinh độ)']:
    list_long.append(i)
  
  for i in df['Lat (Vĩ độ)']:
    list_lat.append(i)

  for i in df['Đài VT']:
    list_group.append(i)
  
  for i in df['Phường/Xã']:
    list_village.append(i)

  for idx,site_id in enumerate(list_site_id):
    site_obj = Site.objects.filter(id = site_id).first()

    if not site_obj:
      print('------',site_id)
      Site(
        id=site_id,
        pmt_code = list_ptm_code[idx],
        district = list_district[idx],
        provice = list_provice[idx],
        address = list_address[idx],
        groups = list_group[idx],
        village = list_village[idx],
        long = list_long[idx],
        lat = list_lat[idx],
      ).save()

  return HttpResponse(1)


def test_get_all_site(request):

  list_sites = Site.objects.all()
    
  return HttpResponse(json.dumps([]))

class SiteMap(TemplateView):
  template_name = 'apps/f_1_2_sites/site-map.html'


  def get_context_data(self, **kwargs) :

    data = super().get_context_data()

    # Data SOH
    list_data_SOH = ACCUData.objects.all()
    dict_SOH = {}
    for i in list_data_SOH:
      if dict_SOH.get(i.site):
        dict_SOH[i.site]['d_dm'] += i.capacity
        dict_SOH[i.site]['d_tt'] += i.dc_current_consumption*(i.backup_time/60)
        # dict_SOH[i.site]['SOH']  = dict_SOH[i.site]['d_tt'] / dict_SOH[i.site]['d_dm']
      else:
        dict_SOH[i.site] = {}
        dict_SOH[i.site]['d_dm'] = i.capacity
        dict_SOH[i.site]['d_tt'] = i.dc_current_consumption*(i.backup_time/60)


    for site in dict_SOH:
      dict_SOH[site]['SOH'] = round((dict_SOH[site]['d_tt'] / dict_SOH[site]['d_dm']) *100) if dict_SOH[site]['d_dm'] else 0
    
    # Data site
    list_sites = Site.objects.all()

    list_site_data = []

    for site in list_sites:
      obj_soh = dict_SOH.get(site.pk)
      if obj_soh:
        status = 'high'
        if obj_soh['SOH'] < 30:
          status = 'low'
        elif obj_soh['SOH'] < 60:
          status = 'medium'

        list_site_data.append({
          'id' : site.pk,
          'long' : str(site.long).replace(',','.'),
          'lat' : str(site.lat).replace(',','.'),
          'status' : status,

        })
    
    list_data_hard_code = [
      {
        "id":"QTDH05",
        "long":"108.2047937",
        "lat":"16.0692333",
        "status": "high" 
      },
      {
        "id":"QTDH22",
        "long":"107.09581",
        "lat":"16.81525",
        "status": "medium"
      },
      {
        "id":"QTVL03",
        "long":"107.106",
        "lat":"17.0217",
        "status": "low",
        "status": "high" 
      },
      {
        "id":"DNHC1V",
        "long":"108.202453",
        "lat":"16.056407",
        "status": "medium" 
      },
      {
          "id":"QATK06",
          "long":"108.46794",
          "lat":"15.57719",
          "status": "low" 
      },
      {
          "id":"DNHV77",
          "long":"108.11089",
          "lat":"16.00946",
          "status": "high" 
      },
      {
          "id":"QATB27",
          "long":"108.43894",
          "lat":"15.67551",
          "status": "medium" 
      },
      {
          "id":"QTHL23",
          "long":"107.21708",
          "lat":"16.67195",
          "status": "low" 
      },
      {
          "id":"DNHW12",
          "long":"108.09774",
          "lat":"15.99625",
          "status": "high" 
      },
      {
          "id":"DNHW16",
          "long":"108.04877",
          "lat":"16.0632",
          "status": "medium" 
      },
      {
          "id":"DNHW18",
          "long":"107.999579",
          "lat":"15.996894",
          "status": "low" 
      },
      {
          "id":"QANT11",
          "long":"108.5740853",
          "lat":"15.487383",
          "status": "high" 
      },
      {
          "id":"QATB10",
          "long":"108.34846",
          "lat":"15.73394",
          "status": "medium" 
      },
      {
          "id":"QATB11",
          "long":"108.34095",
          "lat":"15.7645",
          "status": "low" 
      },
      {
          "id":"QTDK11",
          "long":"106.865",
          "lat":"16.71658",
          "status": "high" 
      },
      {
          "id":"QTDK20",
          "long":"106.9689",
          "lat":"16.312",
          "status": "medium" 
      },
      {
          "id":"QAHA24",
          "long":"108.50033",
          "lat":"15.96954",
          "status": "low" 
      },
      {
          "id":"QTGL47",
          "long":"107.06169",
          "lat":"16.88474",
          "status": "high" 
      },
      {
          "id":"DNCL39",
          "long":"108.18246",
          "lat":"16.00624",
          "status": "medium" 
      },
      {
          "id":"HUHU69",
          "long":"107.54047",
          "lat":"16.44396",
          "status": "low" 
      },
      {
          "id":"QTHH21",
          "long":"106.7466",
          "lat":"16.60213",
          "status": "high" 
      },
      {
          "id":"QTHH44",
          "long":"106.66526",
          "lat":"16.76103",
          "status": "medium" 
      },
      {
          "id":"QADB58",
          "long":"108.28191",
          "lat":"15.87772",
          "status": "low" 
      },
      {
          "id":"DNCL60",
          "long":"108.17295",
          "lat":"16.04621",
          "status": "high" 
      },
      {
          "id":"QTDH38",
          "long":"107.10029",
          "lat":"16.81529",
          "status": "medium" 
      },
      {
          "id":"HUHU70",
          "long":"107.55659",
          "lat":"16.46085",
          "status": "low" 
      },
      {
          "id":"HUHU74",
          "long":"107.580574",
          "lat":"16.478354",
          "status": "high" 
      },
      {
          "id":"HUHU92",
          "long":"107.59661",
          "lat":"16.45792",
          "status": "medium" 
      },
      {
          "id":"QADG10",
          "long":"107.73371",
          "lat":"15.95028",
          "status": "low" 
      },
      {
          "id":"QADG12",
          "long":"107.6427593",
          "lat":"15.8882433",
          "status": "high" 
      },
      {
          "id":"QTGL36",
          "long":"107.1827",
          "lat":"16.90716",
          "status": "medium" 
      },
      {
          "id":"QATM15",
          "long":"108.16362",
          "lat":"15.35399",
          "status": "low" 
      },
      {
          "id":"QAHD12",
          "long":"108.11366",
          "lat":"15.55602",
          "status": "high" 
      },
      {
          "id":"DNLC14",
          "long":"108.14446",
          "lat":"16.07061",
          "status": "medium" 
      },
      {
          "id":"QAPN20",
          "long":"108.4398",
          "lat":"15.5647",
          "status": "low" 
      },
      {
          "id":"QATK27",
          "long":"108.5293",
          "lat":"15.61883",
          "status": "high" 
      },
      {
          "id":"QATB50",
          "long":"108.401489",
          "lat":"15.761197",
          "status": "medium" 
      },
      {
          "id":"DNCL83",
          "long":"108.22971",
          "lat":"16.00935",
          "status": "low" 
      },
      {
          "id":"DNHC89",
          "long":"108.214696",
          "lat":"16.060158",
          "status": "high" 
      },
      {
          "id":"DNLC25",
          "long":"108.15781",
          "lat":"16.07936",
          "status": "medium" 
      }
    ]
    data['list_site_data'] = list_site_data
    # data['list_site_data'] = json.dumps(list_site_data)

    return data


import json,time

from accu_dashboard.models import ACCUTable

MAX_DATA_PER_REQUEST = 200


class MPDMap(TemplateView):
  template_name = 'apps/f_1_2_sites/mpd-map.html'


  def get_context_data(self, **kwargs) :

    data = super().get_context_data()

    param_site_id = self.request.GET.get('site', '-1')
    param_provice = self.request.GET.get('provice', '-1')


    # Get list projive
    list_provices_obj = ACCUTable.objects.values('provice').order_by('provice').distinct()
    list_provices = [i['provice'] for i in list_provices_obj]

    # Data site
    if str(param_provice) != '-1' and str(param_provice).isdigit() and int(param_provice) < len(list_provices)-1:
      list_sites = Site.objects.filter(provice = list_provices[int(param_provice)])
    else:
      list_sites = Site.objects.all()

    list_extract = []
    for i in range(0, len(list_sites), MAX_DATA_PER_REQUEST):
      list_extract.append(list_sites[i : i+MAX_DATA_PER_REQUEST])
    
    

    data['list_site_data'] = []
    data['list_provices'] = list_provices
    data['list_sites'] = list_sites
    data['number_list_data'] = len(list_extract)


    return data

  def post(self, request, *args, **kwargs):

    # Input
    param_action = request.POST.get('action')
    
    # Xử lý load popup
    if param_action == 'get_data_map':
      reponse = self._post_load_add_report_body(request)

    # Trả về
    return reponse
  

  def _post_load_add_report_body(self, request):
    param_data = json.loads(request.POST['params'])
    index_data = param_data['index']

    param_site_id = param_data['site']
    param_provice = param_data['provice']

    # if index_data != 0:
    #   time.sleep(5)

    # Data site
    # list_sites = Site.objects.all()[index_data*MAX_DATA_PER_REQUEST : index_data*MAX_DATA_PER_REQUEST+MAX_DATA_PER_REQUEST]

    # Get list projive
    list_provices_obj = ACCUTable.objects.values('provice').order_by('provice').distinct()
    list_provices = [i['provice'] for i in list_provices_obj]

    # Filter provice
    if str(param_provice) != '-1' and str(param_provice).isdigit() and int(param_provice) < len(list_provices)-1:
      list_sites = Site.objects.filter(provice = list_provices[int(param_provice)])
      print('======', list_provices[int(param_provice)])
      print(len(list_sites))
    else:
      list_sites = Site.objects.all()

    # Filter site
    data_zoom = {}
    if str(param_site_id) != '-1' :
      sites_filter = list_sites.filter(pk=param_site_id).first()

      data_zoom['long'] = str(sites_filter.long).replace(',','.')
      data_zoom['lat'] = str(sites_filter.lat).replace(',','.')
      data_zoom['is_zoom'] = 1

    list_site_data = []



    for site in list_sites:
 
        list_mpd = site.mpdinfo_set.all()
        number_mpd_had_ats = 0
        number_mpd_not_ats = 0

        # list_source = site.cabinetssource_set.all()
        # print(len(list_source))

        for mpd in list_mpd:
          if mpd.atsinfo_set.all():
            number_mpd_had_ats += 1
          else:
            number_mpd_not_ats += 1

        list_site_data.append({
          'id' : site.pk,
          'long' : str(site.long).replace(',','.'),
          'lat' : str(site.lat).replace(',','.'),
          'number_mpd' : len(list_mpd),
          'is_had_mpd' : 1 if list_mpd else 0,
          'number_mpd_had_ats' : number_mpd_had_ats,
          'number_mpd_not_ats' : number_mpd_not_ats,
        })

        if not data_zoom:
          data_zoom['long'] = str(site.long).replace(',','.')
          data_zoom['lat'] = str(site.lat).replace(',','.')
          data_zoom['is_zoom'] = 0

    data = {
      'list_site_data' : list_site_data,
      'data_zoom'  : data_zoom
    }
    return HttpResponse(json.dumps(data))


