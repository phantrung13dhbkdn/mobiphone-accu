import random
from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.contrib.auth.models import Group

# Create your models here.

TYPE_CHOICES = (
  ("coporate","Coporate"),
  ("channel","Channel"),
)
class Site(models.Model):
  id = models.CharField(
    max_length=100,
    unique=True,
    primary_key=True
  )
  pmt_code = models.CharField(max_length=50)
  district = models.CharField(null=True, max_length=255)
  provice = models.CharField(null=True, max_length=255)
  address = models.CharField(null=True, max_length=255)
  groups = models.CharField(null=True, max_length=255)
  village = models.CharField(null=True, max_length=255)
  long = models.CharField(null=True,max_length=255)
  lat = models.CharField(null=True,max_length=255)
  created_date = models.DateTimeField(auto_now_add=True)
  last_updated = models.DateTimeField(auto_now=True)

  
  def __str__(self):
    return f"{self.id}"
  
  class Meta:
    db_table = 'site'
