class LDAPLogin(APIView):
    """
    Class to authenticate a user via LDAP and
    then creating a login session
    """
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        """
        Api to login a user
        :param request:
        :return:
        """
        print('LDAPLogin')
        #user_obj = authenticate(username=request.data['username'],
        #                        password=request.data['password'])
        res = False
        if getattr(settings, 'LDAP_ENABLED', False):
            import ldap
            user_obj = User.objects.filter(username=request.data['username']).first()
            if user_obj is None:
                return Response({'success': False}, status=400)
            try:
                c = ldap.initialize('ldap://mobifone.vn')
                c.set_option(ldap.OPT_NETWORK_TIMEOUT, 5.0)
                c.simple_bind_s(request.data['username']+'@mobifone.vn', request.data['password'])
                res = True
            except:
                res = False
        if res == False:
            user_obj = authenticate(username=request.data['username'], password=request.data['password'])
            if user_obj:
                login(request, user_obj)
                return Response(get_token_from_user(user_obj), status=200)
            return Response({'success': False}, status=400)
        login(request, user_obj)
        return Response(get_token_from_user(user_obj), status=200)
12:56
class LDAPLogin(APIView):
    """
    Class to authenticate a user via LDAP and
    then creating a login session
    """
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        """
        Api to login a user
        :param request:
        :return:
        """
        print('LDAPLogin')
        #user_obj = authenticate(username=request.data['username'],
        #                        password=request.data['password'])
        res = False
        if getattr(settings, 'LDAP_ENABLED', False):
            import ldap
            user_obj = User.objects.filter(username=request.data['username']).first()
            if user_obj is None:
                return Response({'success': False}, status=400)
            try:
                c = ldap.initialize('ldap://mobifone.vn')
                c.set_option(ldap.OPT_NETWORK_TIMEOUT, 5.0)
                c.simple_bind_s(request.data['username']+'@mobifone.vn', request.data['password'])
                res = True
            except:
                res = False
        if res == False:
            user_obj = authenticate(username=request.data['username'], password=request.data['password'])
            if user_obj:
                login(request, user_obj)
                return Response(get_token_from_user(user_obj), status=200)
            return Response({'success': False}, status=400)
        login(request, user_obj)
        return Response(get_token_from_user(user_obj), status=200)