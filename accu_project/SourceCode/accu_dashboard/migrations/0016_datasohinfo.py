# Generated by Django 3.2.8 on 2022-10-23 16:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('f_1_2_sites', '0003_alter_site_id'),
        ('accu_dashboard', '0015_atsinfo_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='DataSOHInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('qr_code', models.CharField(max_length=255, null=True)),
                ('voltage', models.IntegerField(default=0)),
                ('time_backup', models.IntegerField(default=0)),
                ('serial', models.CharField(max_length=255, null=True)),
                ('ird', models.IntegerField(default=0)),
                ('irsx', models.IntegerField(default=0)),
                ('timexa', models.IntegerField(default=0)),
                ('ddm', models.IntegerField(default=0)),
                ('ixa', models.IntegerField(default=0)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('cabinets_source', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accu_dashboard.cabinetssource')),
                ('site', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='f_1_2_sites.site')),
            ],
            options={
                'db_table': 'data_soh_info',
            },
        ),
    ]
