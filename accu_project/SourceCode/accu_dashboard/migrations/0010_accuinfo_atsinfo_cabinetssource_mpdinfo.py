# Generated by Django 3.2.8 on 2022-09-07 17:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accu_dashboard', '0009_auto_20220708_0017'),
    ]

    operations = [
        migrations.CreateModel(
            name='MPDInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('code', models.CharField(max_length=255, null=True)),
                ('product_code', models.CharField(max_length=255, null=True)),
                ('qr_code', models.CharField(max_length=255, null=True)),
                ('wattage', models.IntegerField(default=0)),
                ('phase', models.CharField(max_length=255, null=True)),
                ('parent_code', models.CharField(max_length=255, null=True)),
                ('parent_name', models.CharField(max_length=255, null=True)),
                ('sdate_use', models.DateTimeField(null=True)),
                ('insurance_date', models.DateTimeField(null=True)),
                ('fixing_date', models.DateTimeField(null=True)),
                ('serial_engine', models.CharField(max_length=255, null=True)),
                ('serial_moto', models.CharField(max_length=255, null=True)),
                ('label', models.CharField(max_length=255, null=True)),
                ('status', models.CharField(choices=[(' ', ''), ('changed', 'ĐÃ ĐIỀU CHUYỂN'), ('good', 'HOẠT ĐỘNG TỐT'), ('error', 'HỎNG'), ('error_full', 'HỎNG HOÀN TOÀN'), ('not_used', 'KHÔNG SỬ DỤNG')], default='good', max_length=100)),
                ('cumulative_index', models.CharField(max_length=255, null=True)),
                ('site', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accu_dashboard.siteinfo')),
            ],
            options={
                'db_table': 'mpd_info',
            },
        ),
        migrations.CreateModel(
            name='CabinetsSource',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('code', models.CharField(max_length=255, null=True)),
                ('product_code', models.CharField(max_length=255, null=True)),
                ('qr_code', models.CharField(max_length=255, null=True)),
                ('label', models.CharField(max_length=255, null=True)),
                ('number_module_rectifier', models.IntegerField(default=0)),
                ('rectifier_power', models.IntegerField(default=0)),
                ('sdate_use', models.DateTimeField(null=True)),
                ('serial', models.CharField(max_length=255, null=True)),
                ('device_conect', models.CharField(max_length=255, null=True)),
                ('status', models.CharField(choices=[(' ', ''), ('changed', 'ĐÃ ĐIỀU CHUYỂN'), ('good', 'HOẠT ĐỘNG TỐT'), ('error', 'HỎNG'), ('error_full', 'HỎNG HOÀN TOÀN'), ('not_used', 'KHÔNG SỬ DỤNG')], default='good', max_length=100)),
                ('insurance_date', models.DateTimeField(null=True)),
                ('fixing_date', models.DateTimeField(null=True)),
                ('site', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accu_dashboard.siteinfo')),
            ],
            options={
                'db_table': 'cabinets_source_info',
            },
        ),
        migrations.CreateModel(
            name='ATSInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('code', models.CharField(max_length=255, null=True)),
                ('product_code', models.CharField(max_length=255, null=True)),
                ('qr_code', models.CharField(max_length=255, null=True)),
                ('customer_management', models.CharField(max_length=255, null=True)),
                ('sdate_use', models.DateTimeField(null=True)),
                ('insurance_date', models.DateTimeField(null=True)),
                ('fixing_date', models.DateTimeField(null=True)),
                ('serial', models.CharField(max_length=255, null=True)),
                ('label', models.CharField(max_length=255, null=True)),
                ('status', models.CharField(choices=[(' ', ''), ('changed', 'ĐÃ ĐIỀU CHUYỂN'), ('good', 'HOẠT ĐỘNG TỐT'), ('error', 'HỎNG'), ('error_full', 'HỎNG HOÀN TOÀN'), ('not_used', 'KHÔNG SỬ DỤNG')], default='good', max_length=100)),
                ('mpd', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accu_dashboard.mpdinfo')),
            ],
            options={
                'db_table': 'ats_info',
            },
        ),
        migrations.CreateModel(
            name='ACCUInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('code', models.CharField(max_length=255, null=True)),
                ('product_code', models.CharField(max_length=255, null=True)),
                ('qr_code', models.CharField(max_length=255, null=True)),
                ('voltage', models.IntegerField(default=0)),
                ('battery_capacity', models.IntegerField(default=0)),
                ('number_accu', models.IntegerField(default=0)),
                ('time_backup', models.IntegerField(default=0)),
                ('phase', models.CharField(max_length=255, null=True)),
                ('sdate_use', models.DateTimeField(null=True)),
                ('insurance_date', models.DateTimeField(null=True)),
                ('fixing_date', models.DateTimeField(null=True)),
                ('serial', models.CharField(max_length=255, null=True)),
                ('label', models.CharField(max_length=255, null=True)),
                ('status', models.CharField(choices=[(' ', ''), ('changed', 'ĐÃ ĐIỀU CHUYỂN'), ('good', 'HOẠT ĐỘNG TỐT'), ('error', 'HỎNG'), ('error_full', 'HỎNG HOÀN TOÀN'), ('not_used', 'KHÔNG SỬ DỤNG')], default='good', max_length=100)),
                ('cabinets_source', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accu_dashboard.cabinetssource')),
            ],
            options={
                'db_table': 'accu_info',
            },
        ),
    ]
