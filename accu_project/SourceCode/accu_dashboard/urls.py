from django.urls import path
from django.conf.urls import url, include

from accu_dashboard import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),  
    path('index-ms', views.IndexDashBoard.as_view(), name='index_ms'),  
    path('index-md', views.IndexMD.as_view(), name='index_md'),  
    path('index-mpd', views.IndexMPD.as_view(), name='index_mpd'),  
    path('index-accu', views.IndexACCU.as_view(), name='index_accu'),  
    url(r'^get-data-summary$', views.get_data_summary),
    url(r'^get-data-md$', views.get_data_md),
    url(r'^get-data-mpd$', views.get_data_mpd),
    url(r'^get-data-accu$', views.get_data_accu),
    url(r'^test$', views.test),
    url(r'^import-data$', views.import_data),
    url(r'^import-data-accu$', views.import_data_accu),
    url(r'^import-data-accu-1$', views.import_data_accu_1), #
    url(r'^import-data-accu-2$', views.import_data_accu_2), #

    path('index-log', views.IndexLog.as_view(), name='index_log'),  
    path('index-site', views.IndexDataSOH.as_view(), name='index-site'),
    path('index-site-data', views.IndexDataSOHConvert.as_view(), name='index-site-data'),
    

    url(r'^preview-import-data$', views.preview_data_import),
    url(r'^save-import-data$', views.save_data_import),
    url(r'^del-log$', views.del_log),

    
    # Export data excel
    url(r'^export-data-summary$', views.export_data_summary),
    url(r'^export-data-summary-md$', views.export_data_summary_md),
    url(r'^export-data-summary-mpd$', views.export_data_summary_mpd),
    url(r'^export-data-summary-accu$', views.export_data_summary_accu),

    # Data new
    path('import-data-source-info', views.import_data_source_info),
    path('import-data-accu-info', views.import_data_accu_info),
    path('import-data-mpd-info', views.import_data_mpd_info),
    path('import-data-ats-info', views.import_data_ats_info),

    
    path('mpd-index', views.MPDIndex.as_view(), name='mpd_index'),  
    url(r'^get-data-mpd-info$', views.get_data_mpd_info),
    path('ajax/load-update-mpd-info', views.load_update_mpd_info),
    path('ajax/update-mpd-info', views.update_mpd_info),
    path('mpd/<int:pk>/delete', views.MPDInfoDeleteView.as_view(), name='mpd_info_delete'),    

    # ATS INFO
    path('ats-index', views.ATSIndex.as_view(), name='ats_index'),  
    url(r'^get-data-ats-info$', views.get_data_ats_info),
    path('ajax/load-update-ats-info', views.load_update_ats_info),
    path('ajax/update-ats-info', views.update_ats_info),
    path('ats/<int:pk>/delete', views.ATSInfoDeleteView.as_view(), name='ats_info_delete'), 

    # CabinetsSource 
    path('cabinet-index', views.CabinetIndex.as_view(), name='cabinet_index'),  
    url(r'^get-data-cabinet$', views.get_data_cabinets),
    path('ajax/load-update-cabinet', views.load_update_cabinets),
    path('ajax/update-cabinet', views.update_cabinets),
    path('cabinet/<int:pk>/delete', views.CabinetDeleteView.as_view(), name='cabinet_delete'), 
    
    # ACCU INFO
    path('accu-index', views.ACCUIndex.as_view(), name='accu_index'),  
    url(r'^get-data-accu-info$', views.get_data_accu_info),
    path('ajax/load-update-accu-info', views.load_update_accu_info),
    path('ajax/update-accu-info', views.update_accu_info),
    path('accu/<int:pk>/delete', views.ACCUInfoDeleteView.as_view(), name='accu_info_delete'),

    path('test-cron', views.run_sync_data_r_alarm_log_ms_query),  
    
    path('soh-index', views.DataSOHIndex.as_view(), name='soh_index'),  
    url(r'^get-data-soh-info$', views.get_data_soh_info),
    path('soh/<int:pk>/delete', views.DataSOHInfoDeleteView.as_view(), name='soh_info_delete'),
]  