from django.db import models
from datetime import datetime
from f_1_2_sites.models import Site




# Create your models here.
NETWORK_TYPE_CHOICES = (
  ("2g","2G"),
  ("3g","3G"),
  ("4g","4G"),
  ("5g","5G"),
)

def _get_offset_query_data_table( index=1, pagination=50):
    """Hàm get ra ofset start + offset end khi query data

    Args:
      index (int): index của pagination (1,2,3,4....)
      pagination (int): Limit phân trang (50, 100)
    
    Return : 
    Tuple :  start_index:int, total_record:int
    
    
    """
    start_offset = pagination*(index-1)
    total_record = pagination
    
    return start_offset,total_record

class R_ALARM_LOG(models.Model):
  district = models.CharField(null=True, max_length=255)
  provice = models.CharField(null=True, max_length=255, )
  groups = models.CharField(null=True, max_length=255)
  durations = models.IntegerField(null=True, default=0)
  network = models.CharField(null=True, max_length=255)
  vendor = models.CharField(null=True, max_length=255)
  ne = models.CharField(null=True, max_length=255)
  site = models.CharField(null=True, max_length=255)
  sdate = models.DateTimeField(null=True, verbose_name="")
  edate = models.DateTimeField(null=True, verbose_name="")
  alarm_type = models.CharField(null=True, max_length=255)
  alarm_name = models.CharField(null=True, max_length=255)
  alarm_info = models.CharField(null=True, max_length=2555)

  def get_data_alarm(self, start_date, end_date, page=1, page_size=50):

    sql_query = f"""
      SELECT id,NETWORK,SITE,SDATE,EDATE,DURATIONs,PROVICE,DISTRICT,GROUPS
        FROM accu_dashboard_r_alarm_log 
        where alarm_info like  '%Main AC Alarm%'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    return R_ALARM_LOG.objects.raw(sql_query)

  def get_data_alarm_export(self, start_date, end_date):

    sql_query = f"""
      SELECT id,NETWORK,SITE,SDATE,EDATE,DURATIONs,PROVICE,DISTRICT,GROUPS
        FROM accu_dashboard_r_alarm_log 
        where alarm_info like  '%Main AC Alarm%'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  
      
    """ 

    return R_ALARM_LOG.objects.raw(sql_query)  

  def get_total_data_alarm(self, start_date, end_date):

    sql_query = f"""
      SELECT id
        FROM accu_dashboard_r_alarm_log 
        where alarm_info like  '%Main AC Alarm%'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  
      
    """ 

    return len(R_ALARM_LOG.objects.raw(sql_query))

  def get_data_MD(self, start_date, end_date,page=1,page_size=50):
    sql_query = f"""
      SELECT *
        FROM accu_dashboard_r_alarm_log 
        WHERE alarm_type = 'POWER'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    return R_ALARM_LOG.objects.raw(sql_query)

  def get_data_MD_export(self, start_date, end_date):
    sql_query = f"""
      SELECT *
        FROM accu_dashboard_r_alarm_log 
        WHERE alarm_type = 'POWER'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 

    return R_ALARM_LOG.objects.raw(sql_query)  

  def get_total_data_MD(self, start_date, end_date):
    sql_query = f"""
      SELECT id
        FROM accu_dashboard_r_alarm_log 
        WHERE alarm_type = 'POWER'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 

    return len(R_ALARM_LOG.objects.raw(sql_query))
  
  def get_data_MPD(self, start_date, end_date, page=1, page_size = 50):
    sql_query = f"""
      SELECT id , provice, groups, network, alarm_type , SUM(durations) as total_durations
        FROM accu_dashboard_r_alarm_log 
        WHERE alarm_type = 'GENERATOR'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}') GROUP BY  provice,network ORDER BY durations DESC
      
    """ 
    # start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    # sql_query += f" LIMIT {start_offset }, {total_record}"

    return R_ALARM_LOG.objects.raw(sql_query)

  def get_data_MPD_export(self, start_date, end_date,):
    sql_query = f"""
      SELECT *
        FROM accu_dashboard_r_alarm_log 
        WHERE alarm_type = 'GENERATOR'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 

    return R_ALARM_LOG.objects.raw(sql_query)  

  def get_total_data_MPD(self, start_date, end_date):
    sql_query = f"""
      SELECT id
        FROM accu_dashboard_r_alarm_log 
        WHERE alarm_type = 'GENERATOR'
        and sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 


    return len(R_ALARM_LOG.objects.raw(sql_query))

#  and durations >= {duration_less} and durations <= {duration_more}    
class R_ALARM_LOG_Check(models.Model):
  date = models.DateTimeField(null=True, verbose_name="")
  is_syns = models.BooleanField(default=False)

class ACCUMSData(models.Model):
  district = models.CharField(null=True, max_length=255)
  provice = models.CharField(null=True, max_length=255, )
  groups = models.CharField(null=True, max_length=255)
  network = models.CharField(null=True, max_length=255)
  site = models.CharField(null=True, max_length=255)
  md_sdate = models.DateTimeField(null=True, verbose_name="")
  sdate = models.DateTimeField(null=True, verbose_name="")
  edate = models.DateTimeField(null=True, verbose_name="")


class ACCUTable(models.Model):
  district = models.CharField(null=True, max_length=255)
  provice = models.CharField(null=True, max_length=255, )
  groups = models.CharField(null=True, max_length=255)
  network = models.CharField(null=True, max_length=255)
  site = models.CharField(null=True, max_length=255)
  sdate = models.DateTimeField(null=True, verbose_name="")
  edate = models.DateTimeField(null=True, verbose_name="")
  durations = models.IntegerField(null=True, default=0)

  def get_data_accu(self, start_date, end_date, type_network, duration_less=0, duration_more=30):

    # sql_query = f"""
    #   SELECT id,NETWORK,SITE,SDATE,EDATE,DURATIONs,PROVICE,DISTRICT,GROUPS
    #     FROM accu_data 
    #     where durations >= {duration_less} and durations <= {duration_more}
    #     and sdate >= date('{start_date}')
    #     and sdate <= date('{end_date}')  
    #     and network = '{type_network}'
      
    # """ 

    sql_query = f"""
      SELECT id,NETWORK,SITE,SDATE,EDATE,DURATIONs,PROVICE,DISTRICT,GROUPS
        FROM accu_data 
        where sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  
        and network = '{type_network}'
      
    """ 

    return ACCUTable.objects.raw(sql_query)
  
  def get_data_ACCU(self, start_date, end_date, page=1,page_size=50):
    sql_query = f"""
      SELECT *
        FROM accu_data
        WHERE sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    print(sql_query)
    return ACCUTable.objects.raw(sql_query)

  def get_data_ACCU_export(self, start_date, end_date):
    sql_query = f"""
      SELECT *
        FROM accu_data
        WHERE sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 

    return ACCUTable.objects.raw(sql_query)  

  def get_total_data_ACCU(self, start_date, end_date):
    sql_query = f"""
      SELECT *
        FROM accu_data
        WHERE sdate >= date('{start_date}')
        and sdate <= date('{end_date}')  ORDER BY durations DESC
      
    """ 

    return len(ACCUTable.objects.raw(sql_query))
  
  class Meta:
    db_table = 'accu_data'
  

class SiteInfo(models.Model):
  site = models.CharField(null=True, max_length=255)
  district = models.CharField(null=True, max_length=255)
  type_network = models.CharField("network",max_length=20, choices=NETWORK_TYPE_CHOICES, default="")
  provice = models.CharField(null=True, max_length=255, )
  groups = models.CharField(null=True, max_length=255)

  class Meta:
    db_table = "site_info"


class ACCUDataLog(models.Model):
  file_name = models.CharField(null=True, max_length=255)
  created_date = models.DateTimeField(auto_now_add=True,null=True)

  class Meta:
    db_table = "accu_data_log" 


class ACCUData(models.Model):
  site = models.CharField(null=True, max_length=255)
  number_of_jar = models.IntegerField(null=True)
  brand = models.CharField(null=True, max_length=255)
  voltage = models.IntegerField(null=True)
  capacity = models.IntegerField(null=True)
  backup_time = models.IntegerField(null=True)
  dc_current_consumption = models.FloatField(null=True)
  day_enter_active = models.DateTimeField(null=True)
  log = models.ForeignKey(ACCUDataLog, on_delete=models.SET_NULL, null=True)

  class Meta:
    db_table = "accu_data_import"


class MPDInfo(models.Model):
  name = models.CharField(null=True, max_length=255 )
  code = models.CharField(null=True, max_length=255 )
  product_code = models.CharField(null=True, max_length=255 )
  qr_code = models.CharField(null=True, max_length=255 )
  wattage = models.FloatField(default=0) # Cong suat
  phase = models.CharField(null=True, max_length=255 )
  site = models.ForeignKey(Site, on_delete=models.SET_NULL, null=True)
  parent_code = models.CharField(null=True, max_length=255 )
  parent_name = models.CharField(null=True, max_length=255 )
  sdate_use = models.DateTimeField(null=True)
  insurance_date = models.DateTimeField(null=True) # time bao hanh
  fixing_date = models.DateTimeField(null=True) # time bao duong
  serial_engine = models.CharField(null=True, max_length=255 ) # Serial động cơ
  serial_moto = models.CharField(null=True, max_length=255 ) # Serial đầu phát
  label = models.CharField(null=True, max_length=255 )
  status = models.CharField(
    choices=(
      (' ',''),
      ('changed','ĐÃ ĐIỀU CHUYỂN'),
      ('good','HOẠT ĐỘNG TỐT'),
      ('error','HỎNG'),
      ('error_full','HỎNG HOÀN TOÀN'),
      ('not_used','KHÔNG SỬ DỤNG'),
    ),
    default='good',
    max_length=100
  )

  cumulative_index = models.CharField(null=True, max_length=255 ) #Chỉ số vận hành lũy kế
  

  class Meta:
    db_table = 'mpd_info'

  def get_data_mpd_info(self, page=1,page_size=50):
    sql_query = f"""
      SELECT *
      FROM mpd_info
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    return MPDInfo.objects.raw(sql_query)

  def get_data_mpd_info_export(self):
    sql_query = f"""
      SELECT *
      FROM mpd_info
      
    """ 

    return MPDInfo.objects.raw(sql_query)   



class ATSInfo(models.Model):
  name = models.CharField(null=True, max_length=255 )
  code = models.CharField(null=True, max_length=255 )
  product_code = models.CharField(null=True, max_length=255 )
  qr_code = models.CharField(null=True, max_length=255 )
  customer_management = models.CharField(null=True, max_length=255 )
  mpd = models.ForeignKey(MPDInfo, on_delete=models.SET_NULL, null=True)
  sdate_use = models.DateTimeField(null=True)
  insurance_date = models.DateTimeField(null=True) # time bao hanh
  fixing_date = models.DateTimeField(null=True) # time bao duong
  serial = models.CharField(null=True, max_length=255 )
  label = models.CharField(null=True, max_length=255 )
  type = models.CharField(null=True, max_length=255 )
  status = models.CharField(
    choices=(
      (' ',''),
      ('changed','ĐÃ ĐIỀU CHUYỂN'),
      ('good','HOẠT ĐỘNG TỐT'),
      ('error','HỎNG'),
      ('error_full','HỎNG HOÀN TOÀN'),
      ('not_used','KHÔNG SỬ DỤNG'),
    ),
    default='good',
    max_length=100
  )


  class Meta:
    db_table = 'ats_info'

  def get_data_ats_info(self, page=1,page_size=50):
    sql_query = f"""
      SELECT *
      FROM ats_info
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    return ATSInfo.objects.raw(sql_query)

  def get_data_ats_info_export(self):
    sql_query = f"""
      SELECT *
      FROM ats_info
      
    """ 

    return ATSInfo.objects.raw(sql_query)



class CabinetsSource(models.Model):
  name = models.CharField(null=True, max_length=255 )
  code = models.CharField(null=True, max_length=255 )
  product_code = models.CharField(null=True, max_length=255 )
  site = models.ForeignKey(Site, on_delete=models.SET_NULL, null=True)
  qr_code = models.CharField(null=True, max_length=255 )
  parent_code = models.CharField(null=True, max_length=255 )
  parent_name = models.CharField(null=True, max_length=255 )
  label = models.CharField(null=True, max_length=255 )
  number_rectifier = models.IntegerField(default=0) # Số lượng rectifier
  number_module_rectifier = models.IntegerField(default=0) # Số lượng khe rectifier
  rectifier_power = models.IntegerField(default=0) # Công suất Rectifier (W)
  sdate_use = models.DateTimeField(null=True)
  serial = models.CharField(null=True, max_length=255 ) # Serial tu nguon
  device_conect = models.CharField(null=True, max_length=255 ) # Thiết bị đấu nối vào tủ nguồn
  status = models.CharField(
    choices=(
      (' ',''),
      ('changed','ĐÃ ĐIỀU CHUYỂN'),
      ('good','HOẠT ĐỘNG TỐT'),
      ('error','HỎNG'),
      ('error_full','HỎNG HOÀN TOÀN'),
      ('not_used','KHÔNG SỬ DỤNG'),
    ),
    default='good',
    max_length=100
  )

  insurance_date = models.DateTimeField(null=True) # time bao hanh
  fixing_date = models.DateTimeField(null=True) # time bao duong



  class Meta:
    db_table = 'cabinets_source_info'


  def get_data_cabinets_source_info(self, page=1,page_size=50):
    sql_query = f"""
      SELECT *
      FROM cabinets_source_info
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    return CabinetsSource.objects.raw(sql_query)

  def get_data_cabinets_source_info_export(self):
    sql_query = f"""
      SELECT *
      FROM cabinets_source_info
      
    """ 

    return CabinetsSource.objects.raw(sql_query)

class ACCUInfo(models.Model):
  name = models.CharField(null=True, max_length=255 )
  code = models.CharField(null=True, max_length=255 )
  product_code = models.CharField(null=True, max_length=255 )
  qr_code = models.CharField(null=True, max_length=255 )
  voltage = models.IntegerField(default=0) # dien ap
  battery_capacity = models.IntegerField(default=0) # Dung lượng bình Accu
  number_accu = models.IntegerField(default=0) # so lượng bình Accu
  time_backup = models.IntegerField(default=0) # so lượng bình Accu
  cabinets_source = models.ForeignKey(CabinetsSource, on_delete=models.SET_NULL, null=True)
  sdate_use = models.DateTimeField(null=True)
  insurance_date = models.DateTimeField(null=True) # time bao hanh
  fixing_date = models.DateTimeField(null=True) # time bao duong
  serial = models.CharField(null=True, max_length=255 ) # Serial động cơ
  label = models.CharField(null=True, max_length=255 )
  status = models.CharField(
    choices=(
      (' ',''),
      ('changed','ĐÃ ĐIỀU CHUYỂN'),
      ('good','HOẠT ĐỘNG TỐT'),
      ('error','HỎNG'),
      ('error_full','HỎNG HOÀN TOÀN'),
      ('not_used','KHÔNG SỬ DỤNG'),
    ),
    default='good',
    max_length=100
  )


  class Meta:
    db_table = 'accu_info'

  def get_data_accu_info(self, page=1,page_size=50):
    sql_query = f"""
      SELECT *
      FROM accu_info
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    return ACCUInfo.objects.raw(sql_query)

  def get_data_accu_info_export(self):
    sql_query = f"""
      SELECT *
      FROM accu_info
      
    """ 

    return ACCUInfo.objects.raw(sql_query)


class DataSOHInfo(models.Model):
  name = models.CharField(null=True, max_length=255 )
  qr_code = models.CharField(null=True, max_length=255 )
  voltage = models.IntegerField(default=0) # dòng tải
  time_backup = models.IntegerField(default=0) # so lượng bình Accu
  cabinets_source = models.ForeignKey(CabinetsSource, on_delete=models.SET_NULL, null=True)
  site = models.ForeignKey(Site, on_delete=models.SET_NULL, null=True)
  serial = models.CharField(null=True, max_length=255 ) # Serial động cơ
  ird = models.IntegerField(default=0)
  irsx = models.IntegerField(default=0)
  timexa = models.IntegerField(default=0)
  ddm = models.IntegerField(default=0)
  ixa = models.IntegerField(default=0)
  created_date = models.DateTimeField(auto_now_add=True)
  last_updated = models.DateTimeField(auto_now=True)

  class Meta:
    db_table = 'data_soh_info'

  def get_data_soh_info(self, page=1,page_size=50):
    sql_query = f"""
      SELECT *
      FROM data_soh_info
      
    """ 
    start_offset,total_record = _get_offset_query_data_table(int(page), int(page_size))

    sql_query += f" LIMIT {start_offset }, {total_record}"

    return DataSOHInfo.objects.raw(sql_query)

  def get_data_soh_info_export(self):
    sql_query = f"""
      SELECT *
      FROM data_soh_info
      
    """ 

    return DataSOHInfo.objects.raw(sql_query)