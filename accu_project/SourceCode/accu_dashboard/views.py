from calendar import HTMLCalendar, month
from datetime import date
from math import nan
from re import I

from django.shortcuts import render
from __Config.Include.Common_Include import *
from random import randrange
import pandas as pd
from __Security_Data.data_daily.lib_data_daily.data_daily import DataDaily
from accu_dashboard.models import *
from f_1_2_sites.models import Site
from django.db.models import Sum

from accu_dashboard.handle_excel.export_excel import *

TIME_DURATION_LIST = [{
						"min" : 0,
						"max" : 5,
					},
					{
						"min" : 5,
						"max" : 15,
					},{
						"min" : 15,
						"max" : 30,
					},{
						"min" : 30,
						"max" : 60,
					},{
						"min" : 60,
						"max" : 99999999,
					},
					]


class HomeView(TemplateView):
	template_name = 'apps/accu_dashboard/dashboard.html'

	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		param_type = self.request.GET.get("type",1)
		param_provice = self.request.GET.get("provice","-1")
		param_start_date = self.request.GET.get("start_date","")
		param_end_date = self.request.GET.get("end_date","")

		# Lay thoi gian hine tai
		start_date = datetime.now()
		end_date = datetime.now()
		try:
			current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
			start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
			
		except Exception as inst:
			current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
			start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

		try:
			current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
			end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
			
		except Exception as inst:
			current_end_time =  datetime.now().strftime("%Y-%m-%d")
			end_date = datetime.now().strftime("%Y-%m-%d")

		# Get list projive
		list_provices = ACCUTable.objects.values('provice').order_by('provice').distinct()
		list_provices = [i['provice'] for i in list_provices]
		
		provice_filter = ''
		try:
			if str(param_provice) != '-1' and str(param_provice).isdigit():
				provice_filter = list_provices[int(param_provice)]
		except Exception as inst:
			print('not filter bu provice')

		# Test code
		data['power_off_2G'] = get_data_acccu_custom(start_date, end_date, param_type,"2G", provice_filter)
		data['power_off_3G'] = get_data_acccu_custom(start_date, end_date, param_type,"3G", provice_filter)
		data['power_off_RAN_4G'] = get_data_acccu_custom(start_date, end_date, param_type,"RAN_4G", provice_filter)

		

		data['list_batrerys'] = []
		data["list_provices"] = list_provices
		data["current_start_time"] = current_start_time
		data["current_end_time"] = current_end_time


		# Lay data chart pie
		if str(param_type) == "1":
			count_site_2g = SiteInfo.objects.filter(type_network="2g").exclude(groups__isnull=True).exclude(groups__exact='').count()
			count_site_3g = SiteInfo.objects.filter(type_network="3g").exclude(groups__isnull=True).exclude(groups__exact='').count()
			count_site_4g = SiteInfo.objects.filter(type_network="4g").exclude(groups__isnull=True).exclude(groups__exact='').count()
		
		if str(param_type) == "2":
			count_site_2g = SiteInfo.objects.filter(type_network="2g").exclude(provice__isnull=True).exclude(provice__exact='').count()
			count_site_3g = SiteInfo.objects.filter(type_network="3g").exclude(provice__isnull=True).exclude(provice__exact='').count()
			count_site_4g = SiteInfo.objects.filter(type_network="4g").exclude(provice__isnull=True).exclude(provice__exact='').count()
		
		else:
			count_site_2g = SiteInfo.objects.filter(type_network="2g").exclude(district__isnull=True).exclude(district__exact='').count()
			count_site_3g = SiteInfo.objects.filter(type_network="3g").exclude(district__isnull=True).exclude(district__exact='').count()
			count_site_4g = SiteInfo.objects.filter(type_network="4g").exclude(district__isnull=True).exclude(district__exact='').count()

		count_site_2g_less10 = sum(data["power_off_2G"]['list_time_less_10'])
		count_site_2g_10_30 = sum(data["power_off_2G"]['list_time_10_30'])
		count_site_2g_30_60 = sum(data["power_off_2G"]['list_time_30_60'])
		count_site_2g_more_60 = sum(data["power_off_2G"]['list_time_more_60'])
		total_2g = count_site_2g_less10 + count_site_2g_10_30 +count_site_2g_30_60+count_site_2g_more_60

		count_site_3g_less10 = sum(data["power_off_3G"]['list_time_less_10'])
		count_site_3g_10_30 = sum(data["power_off_3G"]['list_time_10_30'])
		count_site_3g_30_60 = sum(data["power_off_3G"]['list_time_30_60'])
		count_site_3g_more_60 = sum(data["power_off_3G"]['list_time_more_60'])
		total_3g = count_site_3g_less10+count_site_3g_10_30+count_site_3g_30_60+count_site_3g_more_60

		count_site_4g_less10 = sum(data["power_off_RAN_4G"]['list_time_less_10'])
		count_site_4g_10_30 = sum(data["power_off_RAN_4G"]['list_time_10_30'])
		count_site_4g_30_60 = sum(data["power_off_RAN_4G"]['list_time_30_60'])
		count_site_4g_more_60 = sum(data["power_off_RAN_4G"]['list_time_more_60'])
		total_4g = count_site_4g_less10+count_site_4g_10_30+count_site_4g_30_60+count_site_4g_more_60

		dict_data_pie = {
				"data_2g":{
					'list_name': [
					"<10 minutes",
					"10 - 30 minutes",
					"30 - 60 minutes",
					">60 minutes",
					"not raise",
					],
					"list_values":[
							round((count_site_2g_less10/count_site_2g)*100,2),
							round((count_site_2g_10_30/count_site_2g)*100,2),
							round((count_site_2g_30_60/count_site_2g)*100,2),
							round((count_site_2g_more_60/count_site_2g)*100,2),
							round(((count_site_2g-total_2g)/count_site_2g)*100,2)
						]
					},
				"data_3g":{
					'list_name': [
					"<10 minutes",
					"10 - 30 minutes",
					"30 - 60 minutes",
					">60 minutes",
					"not raise",
					],
				"list_values":[
						round((count_site_3g_less10/count_site_3g)*100,2),
						round((count_site_3g_10_30/count_site_3g)*100,2),
						round((count_site_3g_30_60/count_site_3g)*100,2),
						round((count_site_3g_more_60/count_site_3g)*100,2),
						round(((count_site_3g-total_3g)/count_site_3g)*100,2)
					]
				},

				"data_4g":{
					'list_name': [
					"<10 minutes",
					"10 - 30 minutes",
					"30 - 60 minutes",
					">60 minutes",
					"not raise",
					],
				"list_values":[
						round((count_site_4g_less10/count_site_4g)*100,2),
						round((count_site_4g_10_30/count_site_4g)*100,2),
						round((count_site_4g_30_60/count_site_4g)*100,2),
						round((count_site_4g_more_60/count_site_4g)*100,2),
						round(((count_site_4g-total_4g)/count_site_4g)*100,2)
					]
				}

		}
		data["dict_data_pie"] = dict_data_pie

		return data

def get_data_acccu_custom(start_date, end_date, type_location, type_network, provice_filter = ''):
	data_alarm = ACCUTable().get_data_accu(start_date, end_date, type_network)
	data_10 = {}
	data_10_30 = {}
	data_30_60 = {}
	data_60 = {}
	list_data_name = []
	
	for i in data_alarm:
		# Neu tinh theo tinh
		if str(type_location) == '1':
			if i.groups not in list_data_name:
				print(i.groups)
				list_data_name.append(i.groups) 

			if i.durations <= 10:
				if data_10.get(i.groups):
					data_10[i.groups].append(i.site)
				else:
					data_10[i.groups] = [i.site]

			if i.durations > 10 and i.durations <=30:
				if data_10_30.get(i.groups):
					data_10_30[i.groups].append(i.site)
				else:
					data_10_30[i.groups] = [i.site]
			
			if i.durations > 30 and i.durations <=60:
				if data_30_60.get(i.groups):
					data_30_60[i.groups].append(i.site)
				else:
					data_30_60[i.groups] = [i.site]

			if i.durations > 60:
				if data_60.get(i.groups):
					data_60[i.groups].append(i.site)
				else:
					data_60[i.groups] = [i.site]
		elif str(type_location) == "2":
			if i.provice not in list_data_name:
				list_data_name.append(i.provice) 

			if i.durations <= 10:
				if data_10.get(i.provice):
					data_10[i.provice].append(i.site)
				else:
					data_10[i.provice] = [i.site]

			if i.durations > 10 and i.durations <=30:
				if data_10_30.get(i.provice):
					data_10_30[i.provice].append(i.site)
				else:
					data_10_30[i.provice] = [i.site]
			
			if i.durations > 30 and i.durations <=60:
				if data_30_60.get(i.provice):
					data_30_60[i.provice].append(i.site)
				else:
					data_30_60[i.provice] = [i.site]

			if i.durations > 60:
				if data_60.get(i.provice):
					data_60[i.provice].append(i.site)
				else:
					data_60[i.provice] = [i.site]
		
		else:
			# Filter by provice
			if provice_filter:
				if provice_filter != i.provice:
					continue
			if i.district not in list_data_name:
				list_data_name.append(i.district) 

			if i.durations <= 10:
				if data_10.get(i.district):
					data_10[i.district].append(i.site)
				else:
					data_10[i.district] = [i.site]

			if i.durations > 10 and i.durations <=30:
				if data_10_30.get(i.district):
					data_10_30[i.district].append(i.site)
				else:
					data_10_30[i.district] = [i.site]
			
			if i.durations > 30 and i.durations <=60:
				if data_30_60.get(i.district):
					data_30_60[i.district].append(i.site)
				else:
					data_30_60[i.district] = [i.site]

			if i.durations > 60:
				if data_60.get(i.district):
					data_60[i.district].append(i.site)
				else:
					data_60[i.district] = [i.site]		

	list_data_name = list(set(list_data_name))
	list_time_less_10 = []
	list_time_10_30 = []
	list_time_30_60 = []
	list_time_more_60 = []
	for data_name in list_data_name:
		count_less_10 = list(set(data_10.get(data_name,[])))
		list_time_less_10.append(len(count_less_10))

		count_less_10_30 = list(set(data_10_30.get(data_name,[])))
		list_time_10_30.append(len(count_less_10_30))

		count_30_60 = list(set(data_30_60.get(data_name,[])))
		list_time_30_60.append(len(count_30_60))

		count_60 = list(set(data_60.get(data_name,[])))
		list_time_more_60.append(len(count_60))


	dict_data_off_provice = {
		"list_name" : ','.join(list_data_name),
		"list_time_less_10" : list_time_less_10,
		"list_time_10_30" : list_time_10_30,
		"list_time_30_60" : list_time_30_60,
		"list_time_more_60" : list_time_more_60,
	}
	return dict_data_off_provice


class IndexDashBoard(TemplateView):
	template_name = 'apps/accu_dashboard/dashboard-summary.html'

	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		
		return data	


def get_data_summary(request):
	#1) Lấy dữ liệu
	param_start_date = request.POST["start_date"]
	param_end_date = request.POST["end_date"]
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]
	# param_time_duration = request.POST["time_duration"]
	print("param_start_date", param_start_date)
	print("param_end_date", param_end_date)
	# print("param_time_duration", param_time_duration)
	print(param_page)
	print(param_page_size)
	
	# randrange(100)
	list_data_table = []

	#Filter data
	# param_start_date = datetime.strptime(param_start_date,"%Y-%m-%d")
	# param_end_date = datetime.strptime(param_end_date,"%Y-%m-%d")

	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")
	
	data_summary = R_ALARM_LOG().get_data_alarm(start_date, end_date,page=param_page,page_size=param_page_size)
	data = {}
	
	data['list_data_sumary'] = sorted(data_summary, key=lambda x: x.durations, reverse=True)
	data['total'] = R_ALARM_LOG().get_total_data_alarm(start_date, end_date)

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/summary.html'
	return render(request, tmp_file_path, data)

class IndexMD(TemplateView):
	template_name = 'apps/accu_dashboard/dashboard-md.html'

	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()

		data['current_start_time'] = (datetime.now() -timedelta(days=30)).strftime("%Y-%m-%d")
		
		data['current_end_time'] = datetime.now().strftime("%Y-%m-%d")
		
		
		return data	

def get_data_md(request):
	param_start_date = request.POST["start_date"]
	param_end_date = request.POST["end_date"]
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")
	
	# Get dat MD
	data_alarm = R_ALARM_LOG().get_data_MD(start_date, end_date,param_page,param_page_size)
	data_total = R_ALARM_LOG().get_total_data_MD(start_date, end_date)
	list_data_table = data_alarm
	data = {
		'list_data_md' : list_data_table,
		'current_start_time':current_start_time,
		'current_end_time':current_end_time,
		'total' : data_total
	}


	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/time-md.html'
	return render(request, tmp_file_path, data)

class IndexMPD(TemplateView):
	template_name = 'apps/accu_dashboard/dashboard-mpd.html'

	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		data['current_start_time'] = (datetime.now() -timedelta(days=30)).strftime("%Y-%m-%d")
		
		data['current_end_time'] = datetime.now().strftime("%Y-%m-%d")
		
		return data	

def get_data_mpd(request):
	param_start_date = request.POST["start_date"]
	param_end_date = request.POST["end_date"]
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")

	data_alarm = R_ALARM_LOG().get_data_MPD(start_date, end_date)
	# data_total = R_ALARM_LOG().get_total_data_MPD(start_date, end_date)

	list_data_table = data_alarm

	dict_item_chart = {}
	for i in data_alarm:
		if dict_item_chart.get(i.provice):
			dict_item_chart[i.provice][i.network] = i.total_durations
		else:
			dict_item_chart[i.provice] = {
				i.network : i.total_durations
			}


	list_label_chart = []
	list_data_2g = []
	list_data_3g = []
	list_data_4g = []
	list_data_5g = []
	for provice,value in dict_item_chart.items():
		list_label_chart.append(provice)
		
		list_data_2g.append(value.get('2G'))
		list_data_3g.append(value.get('3G'))
		list_data_4g.append(value.get('4G'))
		list_data_5g.append(value.get('5G'))

	data_chart = {
		'list_label_chart' : list_label_chart,
		'list_data_2g' : list_data_2g,
		'list_data_3g' : list_data_3g,
		'list_data_4g' : list_data_4g,
		'list_data_5g' : list_data_5g,
	}
	
	data = {
		'list_data_mpd':list_data_table,
		'current_start_time':current_start_time,
		'current_end_time':current_end_time,
		'data_chart' : json.dumps(data_chart)
		# 'total' : 0
	}

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/time-mpd.html'
	return render(request, tmp_file_path, data)
class IndexACCU(TemplateView):
	template_name = 'apps/accu_dashboard/dashboard-accu.html'

	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		data['current_start_time'] = (datetime.now() -timedelta(days=30)).strftime("%Y-%m-%d")
		
		data['current_end_time'] = datetime.now().strftime("%Y-%m-%d")
		
		return data	

def get_data_accu(request):
	param_start_date = request.POST["start_date"]
	param_end_date = request.POST["end_date"]
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")
	print(param_page,'param_page')
	print(param_page_size,'param_page_size')
	data_alarm = ACCUTable().get_data_ACCU(start_date, end_date,param_page,param_page_size)
	data_total = ACCUTable().get_total_data_ACCU(start_date, end_date)
	print(data_total)
	list_data_table = data_alarm
	data = {
		'list_data_accu':list_data_table,
		'current_start_time':current_start_time,
		'current_end_time':current_end_time,
		'total' : data_total
	}

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/time-accu.html'
	return render(request, tmp_file_path, data)



#==============Test connect Oracle================#
from connect_oracle import OracleDatabase
def test(request):
	db_conect = OracleDatabase()

  # start_date= "01-11-2021"
  # end_date = '01-11-2021'

  # GET_SYNC_DATA_R_ALARM_LOG = f"""
  #  select DISTRICT,PROVINCE,GROUPS,NETWORK,VENDOR,NE,SITE,SDATE,EDATE,ALARM_TYPE,ALARM_NAME,ALARM_INFO from soca.r_alarm_log
  #     where (Sdate >= to_date('{start_date}','dd-mm-yyyy') and (Sdate <= to_date('{end_date}','dd-mm-yyyy') and region = 'MT' and ALARM_NAME like '%Battery-Deep-Discharge%') or
  #     (Sdate >= to_date('{start_date}','dd-mm-yyyy') and Sdate <= to_date('{end_date}','dd-mm-yyyy') and region = 'MT' and ALARM_INFO like '%Low battery Alarm%')or
  #     (Sdate >= to_date('{start_date}','dd-mm-yyyy') and Sdate <= to_date('{end_date}','dd-mm-yyyy') and region = 'MT' and ALARM_TYPE in ('POWER','GENERATOR')))

  # """
  # print(GET_SYNC_DATA_R_ALARM_LOG)
  # data_db = db_conect.run_query(GET_SYNC_DATA_R_ALARM_LOG)
  # print("testtttttt", len(data_db))
	# print("start-----", datetime.now())

	# test_get_date = """
	# 	select id,SDATE,EDATE from soca.r_alarm_log
	# 		where ( region = 'MT' and ALARM_NAME like '%Battery-Deep-Discharge%') or 
	# 		(region = 'MT' and ALARM_INFO like '%Low battery Alarm%')or
	# 		(region = 'MT' and ALARM_TYPE in ('POWER','GENERATOR'))
	# 			order by Sdate asc  """

	# data_db = db_conect.run_query(test_get_date)
	# print(data_db[1])

	# for i in data_db:
	# 	SDATE = i["SDATE"].strftime("%d-%m-%Y")
	# 	if not R_ALARM_LOG_Check.objects.filter(date=SDATE).exists():
	# 		R_ALARM_LOG_Check(
	# 			date = datetime.strptime(SDATE,"%d-%m-%Y" )
	# 		).save()

	# 	EDATE = i["EDATE"].strftime("%d-%m-%Y")
	# 	if not R_ALARM_LOG_Check.objects.filter(date=EDATE).exists():
	# 		R_ALARM_LOG_Check(
	# 			date = datetime.strptime(EDATE,"%d-%m-%Y" )
	# 		).save()

	# print("end-----")
	# return HttpResponse(1)









	data = []
#   dd = DataDaily()
	# list_date = R_ALARM_LOG_Check.objects.filter(is_syns=False)
	list_date = []
	start_date = datetime.strptime("01-01-2019", "%d-%m-%Y" )
	while start_date < datetime.now():
		list_date.append(start_date)
		start_date = start_date + timedelta(1)

	print("count of date:" , len(list_date))

	for i in list_date:
		if  R_ALARM_LOG_Check.objects.filter(date=i).exists():
			print("Skip date : ",date )
			continue
		date = i
		date_up = date + timedelta(1)
		print("checking for : ",date )

		datas = sync_data_r_alarm_log(date.strftime("%d-%m-%Y"), date_up.strftime("%d-%m-%Y"))

		# save data 
		for sub_data in datas:
			R_ALARM_LOG(
				district = sub_data["DISTRICT"],
				provice = sub_data["PROVINCE"],
				groups = sub_data["GROUPS"],
				network = sub_data["NETWORK"],
				vendor = sub_data["VENDOR"],
				durations = sub_data["DURATION"],
				ne = sub_data["NE"],
				site = sub_data["SITE"],
				sdate = sub_data["SDATE"],
				edate = sub_data["EDATE"],
				alarm_type = sub_data["ALARM_TYPE"],
				alarm_name = sub_data["ALARM_NAME"],
				alarm_info = sub_data["ALARM_INFO"],
			).save()

		R_ALARM_LOG_Check(
				date = date, is_syns = True
			).save()
		# i.is_syns = True
		# i.save()




	print("trsult ; ", len(data))
	print(data[1])
  
	return HttpResponse(json.dumps(data))

def sync_data_r_alarm_log_ms_query(date, date_up):
  db_conect = OracleDatabase()
  GET_SYNC_DATA_R_ALARM_LOG_MS = f"""
	  SELECT SITE_ID, to_date(SDATE, 'DD-MM-YYYY HH24:MI:SS') BAT_DAU, to_date(EDATE, 'DD-MM-YYYY HH24:MI:SS') KET_THUC,Round((EDATE-SDATE)*1440,2) Thoi_gian_MLL,ALARM_NAME,
		PROVINCE, DISTRICT, MA_PHONG_XL as GROUPS, NETWORK, NN_CAP_1,NN_CAP_2, NN_CAP_3, MD_SDATE Thoi_gian_mat_dien, Round((EDATE-SDATE)*1440,2) DURATION, region  TTML
		FROM soca.R_ALARM_SITE_MLL
		where SDATE >= to_date('{date}','dd-mm-yyyy') AND SDATE < to_date('{date_up}','dd-mm-yyyy') AND region = 'MT'

  """

  data_db = db_conect.run_query(GET_SYNC_DATA_R_ALARM_LOG_MS)
  return data_db

def run_sync_data_r_alarm_log_ms_query(request):

	list_date_checked = R_ALARM_LOG_Check.objects.filter()
	last_updated = list(list_date_checked)[-1].date

	today = datetime.now().astimezone()
	number_day_need_update = (today  - last_updated).days 
	print(f'---------NEED update data for : {number_day_need_update} days')

	for i in range(number_day_need_update):

		update_time = today - timedelta(i)

		if R_ALARM_LOG_Check.objects.filter(date=update_time).exists():
			print('-----Skip data for ', update_time)

			continue
		else:
			print('-----Update data for ', update_time)

			print("===== Update data MS=========")
			# dd-mm-yyyy
			list_data_ms = sync_data_r_alarm_log(	date=update_time.strftime('%d-%m-%y'),
															date_up=update_time.strftime('%d-%m-%y'))
			
			for data_ms in list_data_ms:
				try:
					obj_data, is_created = R_ALARM_LOG.objects.get_or_create(
						district = data_ms["DISTRICT"],
						provice = data_ms["PROVINCE"],
						groups = data_ms["GROUPS"],
						network = data_ms["NETWORK"],
						vendor = data_ms["VENDOR"],
						durations = round((data_ms["EDATE"] - data_ms["SDATE"]).days * 1400 , 2),
						ne = data_ms["NE"],
						site = data_ms["SITE"],
						sdate = data_ms["SDATE"],
						edate = data_ms["EDATE"],
						alarm_type = data_ms["ALARM_TYPE"],
						alarm_name = data_ms["ALARM_NAME"],
						alarm_info = data_ms["ALARM_INFO"],
					)
				except Exception as inst:
					print("=========errorrr==========", inst)
				print(f'is_created : {is_created} of date {update_time}')

			R_ALARM_LOG_Check(
					date = date, is_syns = True
				).save()





def sync_data_r_alarm_log(date, date_up):
  db_conect = OracleDatabase()

#   GET_SYNC_DATA_R_ALARM_LOG = f"""
#    select *,Round((EDATE-SDATE)*1440,2) DURATION,ALARM_TYPE,ALARM_NAME,ALARM_INFO from soca.r_alarm_log
#       where (Sdate >= to_date('{date}','dd-mm-yyyy') and (Sdate <= to_date('{date_up}','dd-mm-yyyy') and region = 'MT' and ALARM_NAME like '%Battery-Deep-Discharge%') or
#       (Sdate >= to_date('{date}','dd-mm-yyyy') and Sdate <= to_date('{date_up}','dd-mm-yyyy') and region = 'MT' and ALARM_INFO like '%Low battery Alarm%')or
#       (Sdate >= to_date('{date}','dd-mm-yyyy') and Sdate <= to_date('{date_up}','dd-mm-yyyy') and region = 'MT' and ALARM_TYPE in ('POWER','GENERATOR')))

#   """

  GET_SYNC_DATA_R_ALARM_LOG = f"""
   select * from soca.r_alarm_log
      where (SDATE >= to_date('{date}','dd-mm-yyyy') and SDATE <= to_date('{date_up}','dd-mm-yyyy') and region = 'MT' and ALARM_NAME like '%Battery-Deep-Discharge%') or
      (SDATE >= to_date('{date}','dd-mm-yyyy') and SDATE <= to_date('{date_up}','dd-mm-yyyy') and region = 'MT' and ALARM_INFO like '%Low battery Alarm%')or
      (SDATE >= to_date('{date}','dd-mm-yyyy') and SDATE <= to_date('{date_up}','dd-mm-yyyy') and region = 'MT' and ALARM_TYPE in ('POWER','GENERATOR'))

  """

  data_db = db_conect.run_query(GET_SYNC_DATA_R_ALARM_LOG)
  return data_db

def sync_data_r_alarm_log_mpd(date, date_up):
  db_conect = OracleDatabase()
  GET_SYNC_DATA_R_ALARM_LOG_MPD = f"""
	  SELECT *,Round((EDATE-SDATE)*1440,2) DURATION
		FROM soca.R_ALARM_LOG
		where SDATE >= to_date('{date}','dd-mm-yyyy')  AND SDATE < to_date('{date_up}','dd-mm-yyyy')
		and region = 'MT'  and alarm_info like '%Generator%'

  """

  data_db = db_conect.run_query(GET_SYNC_DATA_R_ALARM_LOG_MPD)
  return data_db

def sync_data_r_alarm_log_accu(date, date_up):
  db_conect = OracleDatabase()
  GET_SYNC_DATA_R_ALARM_LOG_ACCU_1 = f"""
	  SELECT NETWORK,SITE_ID,MD_SDATE,SDATE,Round((SDATE-MD_SDATE)*1440,2) DURATION,PROVINCE,DISTRICT
		FROM soca.R_ALARM_SITE_MLL
		where SDATE >= to_date('{date}','dd-mm-yyyy')  AND SDATE <  to_date('{date_up}','dd-mm-yyyy')
		 AND MD_SDATE is not null and region = 'MT' and SDATE-MD_SDATE <= 360


  """


  GET_SYNC_DATA_R_ALARM_LOG_ACCU_2 = f"""
	SELECT  NETWORK,SITE,SDATE,EDATE,Round((EDATE-SDATE)*1440,2) DURATION,PROVINCE,DISTRICT  FROM soca.R_ALARM_LOG
		where SDATE >= to_date('{date}','dd-mm-yyyy') AND SDATE <  to_date('{date_up}','dd-mm-yyyy')
		and region = 'MT'  and alarm_info like  '%MAIN AC ALARM%' 
		and (EDATE-SDATE)*1440 >= 60 and (EDATE-SDATE)*1440 <= 360

  """

  data_db1 = db_conect.run_query(GET_SYNC_DATA_R_ALARM_LOG_ACCU_1)
  data_db2 = db_conect.run_query(GET_SYNC_DATA_R_ALARM_LOG_ACCU_2)

  return list(list(data_db1) + list(data_db2))



import openpyxl
import pandas as pd
from django.db import transaction

def import_data(request):
	"""
	T1-62019.xlsx
	
	"""
	path_file = "C:/ACCU-master/ACCU-master/ACCU/data/T11-T12 2019.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')


	with transaction.atomic():
		for idx, data in enumerate(excel_data_df["DISTRICT"]):
			try:
				print(round(idx/len(excel_data_df["DISTRICT"]),2 )*100, "-------%---------")
			except Exception as inst:
				print(inst)
			disttrict = data
			provice = excel_data_df["PROVINCE"][idx]
			groups = excel_data_df["DEPT"][idx]
			durations = excel_data_df["DURATION"][idx]
			network = excel_data_df["NETWORK"][idx]
			vendor = excel_data_df["VENDOR"][idx]
			ne = excel_data_df["NE"][idx]
			site = excel_data_df["SITE"][idx]
			sdate = excel_data_df["SDATE"][idx]
			edate = excel_data_df["EDATE"][idx]
			alarm_type = excel_data_df["ALARM_TYPE"][idx]
			alarm_name = excel_data_df["ALARM_NAME"][idx]
			alarm_info = excel_data_df["ALARM_INFO"][idx]

			R_ALARM_LOG(
				district = disttrict,
				provice = provice,
				groups = groups,
				network = network,
				vendor = vendor,
				durations = durations,
				ne = ne,
				site = site,
				sdate = sdate,
				edate = edate,
				alarm_type = alarm_type,
				alarm_name = alarm_name,
				alarm_info = alarm_info,
			).save()

		


	

	print("trung")
	return HttpResponse(1)

def import_data_accu(request):
	path_file = "C:/ACCU-master/ACCU-master/ACCU/data/MS.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')

	print(excel_data_df)
	for idx, data in enumerate(excel_data_df["DISTRICT"]):
		try:
			print(round(idx/len(excel_data_df["DISTRICT"]),2 )*100, "-------%---------")
		except Exception as inst:
			print(inst)
		
		
		disttrict = data
		provice = excel_data_df["PROVINCE"][idx]
		groups = excel_data_df["DEPT"][idx]
		network = excel_data_df["NETWORK"][idx]
		site = excel_data_df["SITE_ID"][idx]
		sdate = excel_data_df["SDATE"][idx]
		edate = excel_data_df["EDATE"][idx]
		md_sdate = excel_data_df["MD_SDATE"][idx] if str(excel_data_df["MD_SDATE"][idx]) != "nan" else None

		print(md_sdate)
		ACCUMSData(
			district = disttrict,
			provice = provice,
			groups = groups,
			network = network,
			site = site,
			sdate = sdate,
			edate = edate,
			md_sdate = md_sdate
		).save()

		
	return HttpResponse(1)

from dateutil import parser
def import_data_accu_1(request):

	path_file = "C:/ACCU-master/ACCU-master/ACCU/data/Bangacquy.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')

	print(excel_data_df)
	for idx, data in enumerate(excel_data_df["DISTRICT"]):
		try:
			print(round(idx/len(excel_data_df["DISTRICT"]),2 )*100, "-------%---------")
		except Exception as inst:
			print(inst)
		
		
		disttrict = data
		provice = excel_data_df["PROVINCE"][idx]
		groups = excel_data_df["GROUPS"][idx]
		network = excel_data_df["NETWORK"][idx]
		site = excel_data_df["SITE"][idx]
		sdate = excel_data_df["SDATE"][idx]
		edate = excel_data_df["EDATE"][idx]
		duration = excel_data_df["DURATION"][idx] 
		print(duration, sdate, type(sdate))
		ACCUTable(
			district = disttrict,
			provice = provice,
			groups = groups,
			network = network,
			site = site,
			sdate = parser.parse(sdate),
			edate = parser.parse(edate),
			durations = duration
		).save()

		
	return HttpResponse(1)


def import_data_accu_2(request):

	path_file = "C:/ACCU-master/ACCU-master/ACCU/data/thongtin2g3g4g.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')

	print(excel_data_df)
	for idx, data in enumerate(excel_data_df["SITENAME"]):
		try:
			print(round(idx/len(excel_data_df["SITENAME"]),2 )*100, "-------%---------")
		except Exception as inst:
			print(inst)
		
		
		site = data
		is_2g = excel_data_df["Trạm 2G"][idx]
		is_3g = excel_data_df["Trạm 3G"][idx]
		is_4g = excel_data_df["Trạm 4G"][idx]
		groups = excel_data_df["dai"][idx]
		provice = excel_data_df["tinh"][idx]
		district = excel_data_df["huyen"][idx] 
		if str(is_2g) == "x":
			SiteInfo(
				site=site,
				district=district,
				groups=groups,
				provice=provice,
				type_network="2g"
			).save()
		if str(is_3g) == "x":
			SiteInfo(
				site=site,
				district=district,
				groups=groups,
				provice=provice,
				type_network="3g"
			).save()
		if str(is_4g) == "x":
			SiteInfo(
				site=site,
				district=district,
				groups=groups,
				provice=provice,
				type_network="4g"
			).save()

		
	return HttpResponse(1)


def export_data_summary(request):
	param_start_date = request.GET["start_date"]
	param_end_date = request.GET["end_date"]
	# param_time_duration = request.POST["time_duration"]
	print("param_start_date", param_start_date)
	print("param_end_date", param_end_date)
	# print("param_time_duration", param_time_duration)

	
	# randrange(100)
	list_data_table = []

	#Filter data
	# param_start_date = datetime.strptime(param_start_date,"%Y-%m-%d")
	# param_end_date = datetime.strptime(param_end_date,"%Y-%m-%d")

	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")
	
	data_summary = R_ALARM_LOG().get_data_alarm_export(start_date, end_date)
	data = {}
	
	data['list_data_sumary'] = sorted(data_summary, key=lambda x: x.durations, reverse=True)
	
	response = draw_excel_summary(data['list_data_sumary'])

	return response

def export_data_summary_md(request):
	param_start_date = request.GET["start_date"]
	param_end_date = request.GET["end_date"]
	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")
	
	# Get dat MD
	data_alarm = R_ALARM_LOG().get_data_MD_export(start_date, end_date)

	list_data_table = data_alarm
	
	response = draw_excel_summary_md(list_data_table)

	return response

def export_data_summary_mpd(request):
	param_start_date = request.GET["start_date"]
	param_end_date = request.GET["end_date"]
	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")

	data_alarm = R_ALARM_LOG().get_data_MPD_export(start_date, end_date)

	list_data_table = data_alarm
	
	response = draw_excel_summary_mpd(list_data_table)

	return response

def export_data_summary_accu(request):
	param_start_date = request.GET["start_date"]
	param_end_date = request.GET["end_date"]
	# Lay thoi gian hine tai
	start_date = datetime.now()
	end_date = datetime.now()
	try:
		current_start_time = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		start_date = datetime.strptime(param_start_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_start_time =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
		start_date =  (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")

	try:
		current_end_time = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		end_date = datetime.strptime(param_end_date,"%Y-%m-%d").strftime("%Y-%m-%d")
		
	except Exception as inst:
		current_end_time =  datetime.now().strftime("%Y-%m-%d")
		end_date = datetime.now().strftime("%Y-%m-%d")

	data_alarm = ACCUTable().get_data_ACCU_export(start_date, end_date)

	list_data_table = data_alarm
	
	response = draw_excel_summary_accu(list_data_table)

	return response


class IndexLog(TemplateView):
	template_name = 'apps/accu_dashboard/index-log.html'

	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		data['current_start_time'] = (datetime.now() -timedelta(days=30)).strftime("%Y-%m-%d")
		
		data['current_end_time'] = datetime.now().strftime("%Y-%m-%d")

		data['link_example'] = '/static/csv/data_example.xlsx'
		
		list_log = ACCUDataLog.objects.all()
		for log_info in list_log:
			log_info.number_data = len(ACCUData.objects.filter(log=log_info))
		data['list_log'] = list_log
		
		return data	

class IndexDataSOH(TemplateView):
	template_name = 'apps/accu_dashboard/index-data-soh.html'

	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		data['current_start_time'] = (datetime.now() -timedelta(days=30)).strftime("%Y-%m-%d")
		
		data['current_end_time'] = datetime.now().strftime("%Y-%m-%d")
		
		list_data_site_soh = ACCUData.objects.all()
		# list_data_soh_convert = []

		# for data_soh in list_data_site_soh:
		# 	list_data_soh_convert.append({

		# 	})
		data['list_data_soh'] = list_data_site_soh

		print(list_data_site_soh)
		
		return data	

def preview_data_import(request):
	# Process xử lý load file csv
	#2) Đọc file
	files = request.FILES
	
	type_import = request.POST['type_import']
	print(type_import,'type_import')
	content_file = files["file"]
	# type_import = request.POST
	#4) Lấy data từ file
	df = pd.read_excel(content_file)
	
	# thread = ThreadPool(processes=5)
	list_data = []
	response = 1
	if type_import == "mpd_info":

		try:
			for i in df.index:
				header_1 = df['SITE_ID'][i]
				header_2 = df['Mã đối tượng cha'][i]
				header_3 = df['Tên đối tượng cha'][i]
				header_4 = df['Mã đối tượng'][i]
				header_5 = df['Tên đối tượng'][i]
				header_6 = df['Mã QR'][i]
				header_7 = df['Nhãn hiệu Máy phát điện'][i]
				header_8 = df['Công suất Máy phát điện'][i]
				header_9 = df['Phase'][i]
				header_10 = df['Serial động cơ'][i]
				header_11 = df['Serial đầu phát'][i]
				header_12 = df['Ngày đưa vào sử dụng'][i]
				header_13 = df['Trạng thái'][i]
				header_14 = df['Thời hạn bảo hành'][i]
				header_15 = df['Thời hạn bảo dưỡng'][i]
				header_16 = df['Chỉ số vận hành lũy kế'][i]
				header_17 = df['Product_code máy phát điện'][i]

				

				data_info = {
					'header_1' : header_1,
					'header_2' : header_2,
					'header_3' : header_3,
					'header_4' : header_4,
					'header_5' : header_5,
					'header_6' : header_6,
					'header_7' : header_7,
					'header_8' : header_8,
					'header_9' : header_9,
					'header_10' : header_10,
					'header_11' : header_11,
					'header_12' : header_12,
					'header_13' : header_13,
					'header_14' : header_14,
					'header_15' : header_15,
					'header_16' : header_16,
					'header_17' : header_17,
					
				} 
				
				list_data.append(data_info)
		except Exception as inst:
			print(inst)
			response = 0		

	elif type_import == "ats_info":

		try:
			for i in df.index:
				header_1 = df['SITE_ID'][i]
				header_2 = df['Mã đối tượng cha'][i]
				header_3 = df['Tên đối tượng cha'][i]
				header_4 = df['Mã đối tượng'][i]
				header_5 = df['Tên đối tượng'][i]
				header_6 = df['Mã QR'][i]
				header_7 = df['Đơn vị chủ quản ATS'][i]
				header_8 = df['Ngày đưa vào sử dụng'][i]
				header_9 = df['Serial ATS'][i]
				header_10 = df['Nhãn hiệu ATS'][i]
				header_11 = df['Loại TB ATS'][i]
				header_12 = df['Trạng thái'][i]
				header_13 = df['Product_code ATS'][i]
				header_14 = df['Thời hạn bảo dưỡng'][i]
				header_15 = df['Thời hạn bảo hành'][i]

				

				data_info = {
					'header_1' : header_1,
					'header_2' : header_2,
					'header_3' : header_3,
					'header_4' : header_4,
					'header_5' : header_5,
					'header_6' : header_6,
					'header_7' : header_7,
					'header_8' : header_8,
					'header_9' : header_9,
					'header_10' : header_10,
					'header_11' : header_11,
					'header_12' : header_12,
					'header_13' : header_13,
					'header_14' : header_14,
					'header_15' : header_15,
					
				} 
				
				list_data.append(data_info)
		except Exception as inst:
			print(inst)
			response = 0

	elif type_import == "cabinet":

		try:
			for i in df.index:
				header_1 = df['SITE_ID'][i]
				header_2 = df['Mã đối tượng cha'][i]
				header_3 = df['Tên đối tượng cha'][i]
				header_4 = df['Mã đối tượng'][i]
				header_5 = df['Tên đối tượng'][i]
				header_6 = df['Mã QR'][i]
				header_7 = df['Nhãn hiệu Tủ nguồn'][i]
				header_8 = df['Số lượng khe rectifier'][i]
				header_9 = df['Số lượng rectifier'][i]
				header_10 = df['Công suất Rectifier (W)'][i]
				header_11 = df['Số lượng Bình Accu'][i]
				header_12 = df['Ngày đưa vào sử dụng'][i]
				header_13 = df['Serial tủ nguồn'][i]
				header_14 = df['Thiết bị đấu nối vào tủ nguồn'][i]
				header_15 = df['Trạng thái'][i]
				header_16 = df['Product_code tủ nguồn'][i]
				header_17 = df['Thời hạn bảo dưỡng'][i]
				header_18 = df['Thời hạn bảo hành'][i]

				

				data_info = {
					'header_1' : header_1,
					'header_2' : header_2,
					'header_3' : header_3,
					'header_4' : header_4,
					'header_5' : header_5,
					'header_6' : header_6,
					'header_7' : header_7,
					'header_8' : header_8,
					'header_9' : header_9,
					'header_10' : header_10,
					'header_11' : header_11,
					'header_12' : header_12,
					'header_13' : header_13,
					'header_14' : header_14,
					'header_15' : header_15,
					'header_16' : header_16,
					'header_17' : header_17,
					'header_18' : header_18,
					
				} 
				
				list_data.append(data_info)
		except Exception as inst:
			print(inst)
			response = 0	

	elif type_import == "accu_info":

		try:
			for i in df.index:
				header_1 = df['SITE_ID'][i]
				header_2 = df['Mã đối tượng cha'][i]
				header_3 = df['Tên đối tượng cha'][i]
				header_4 = df['Mã đối tượng'][i]
				header_5 = df['Tên đối tượng'][i]
				header_6 = df['Mã QR'][i]
				header_7 = df['Trạng thái'][i]
				header_8 = df['Điện áp Accu'][i]
				header_9 = df['Product_code ACCU'][i]
				header_10 = df['Nhãn hiệu Accu'][i]
				header_11 = df['Dung lượng bình Accu'][i]
				header_12 = df['Thời hạn bảo dưỡng'][i]
				header_13 = df['Số lượng Bình Accu'][i]
				header_14 = df['Thời hạn bảo hành'][i]
				header_15 = df['Thời gian Backup'][i]
				header_16 = df['Serial ACCU'][i]

				data_info = {
					'header_1' : header_1,
					'header_2' : header_2,
					'header_3' : header_3,
					'header_4' : header_4,
					'header_5' : header_5,
					'header_6' : header_6,
					'header_7' : header_7,
					'header_8' : header_8,
					'header_9' : header_9,
					'header_10' : header_10,
					'header_11' : header_11,
					'header_12' : header_12,
					'header_13' : header_13,
					'header_14' : header_14,
					'header_15' : header_15,
					'header_16' : header_16
					
				} 
				
				list_data.append(data_info)
		except Exception as inst:
			print(inst)
			response = 0			

	elif type_import == "soh_info":

		try:
			for i in df.index:
				header_1 = df['SITE_ID'][i]
				header_2 = df['Mã đối tượng tủ nguồn'][i]
				header_3 = df['Tên đối tượng'][i]
				header_4 = df['Mã QR'][i]
				header_5 = df['Serial tủ nguồn'][i]
				header_6 = df['Thời gian backup'][i]
				header_7 = df['Dòng tải'][i]
				header_8 = df['IRD'][i]
				header_9 = df['IRSX'][i]
				header_10 = df['IXA'][i]
				header_11 = df['TIMEXA'][i]
				header_12 = df['DDM'][i]


				data_info = {
					'header_1' : header_1,
					'header_2' : header_2,
					'header_3' : header_3,
					'header_4' : header_4,
					'header_5' : header_5,
					'header_6' : header_6,
					'header_7' : header_7,
					'header_8' : header_8,
					'header_9' : header_9,
					'header_10' : header_10,
					'header_11' : header_11,
					'header_12' : header_12,
					
				} 
				
				list_data.append(data_info)
		except Exception as inst:
			print(inst)
			response = 0	

	data = {
		"list_data" : list_data,
		"response" : response,
		'type_import': type_import
	}
	         
	return render(request,'apps/accu_dashboard/modal/table_preview_data.html',data)	


def save_data_import(request):
	files = request.FILES
	content_file = files["file"]
	type_import = request.POST['type_import']
	
	#4) Lấy data từ file
	excel_data_df = pd.read_excel(content_file, engine='openpyxl')
	
	def _import_data_accu_info(excel_data_df):
		
		for idx, data in enumerate(excel_data_df["SITE_ID"]):
			try:
				print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
			except Exception as inst:
				print(inst)
						
			# site_id = data
			parent_code = excel_data_df["Mã đối tượng cha"][idx]
			code = excel_data_df["Mã đối tượng"][idx]
			name = excel_data_df["Tên đối tượng"][idx]
			qr_code = excel_data_df["Mã QR"][idx]
			voltage = excel_data_df["Điện áp Accu"][idx] if str(excel_data_df["Điện áp Accu"][idx]) != "nan" else 0
			status = excel_data_df["Trạng thái"][idx] if str(excel_data_df["Trạng thái"][idx]) != "nan" else ''
			product_code = excel_data_df["Product_code ACCU"][idx]
			label = excel_data_df["Nhãn hiệu Accu"][idx]
			battery_capacity = excel_data_df["Dung lượng bình Accu"][idx] if str(excel_data_df["Dung lượng bình Accu"][idx]) != "nan" else 0
			fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None
			number_accu = excel_data_df["Số lượng Bình Accu"][idx]  if str(excel_data_df["Số lượng Bình Accu"][idx]) != "nan" else 0
			insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
			time_backup = excel_data_df["Thời gian Backup"][idx] if str(excel_data_df["Thời gian Backup"][idx]) != "nan" else 0
			serial = excel_data_df["Serial ACCU"][idx]	
			print(battery_capacity)

			battery_capacity = re.findall(r'\d+', str(battery_capacity))
			battery_capacity = battery_capacity[0] if battery_capacity else 0

			parent_code = CabinetsSource.objects.filter(code = parent_code).first()

			dict_status = {
				'' : ' ',
				'ĐÃ ĐIỀU CHUYỂN' : 'changed',
				'HOẠT ĐỘNG TỐT' : 'good',
				'HỎNG' : 'error',
				'HỎNG HOÀN TOÀN' : 'error_full',
				'KHÔNG SỬ DỤNG' : 'not_used',
			}

			if insurance_date:
				insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')

			if fixing_date:
				fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

			print(dict_status.get(status), status)
			author, created = ACCUInfo.objects.get_or_create(
				name = name,
				code = code,
				cabinets_source = parent_code,
				qr_code = qr_code,
				voltage = voltage,
				battery_capacity = battery_capacity,
				label = label,
				status = dict_status.get(status),
				fixing_date = fixing_date,
				number_accu = number_accu,
				insurance_date = insurance_date,
				time_backup = time_backup,
				serial = serial,
				product_code = product_code,
			)

	def _import_data_mpd_info(excel_data_df):
		list_site_not_found = []
		for idx, data in enumerate(excel_data_df["SITE_ID"]):
			try:
				print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
			except Exception as inst:
				print(inst)

			site_id = Site.objects.filter(id=data).first()

			if not site_id:
				print(' not found site ', data)
				list_site_not_found.append(data)
				continue

			parent_code = excel_data_df["Mã đối tượng cha"][idx]
			parent_name = excel_data_df["Tên đối tượng cha"][idx]
			code = excel_data_df["Mã đối tượng"][idx]
			name = excel_data_df["Tên đối tượng"][idx]
			qr_code = excel_data_df["Mã QR"][idx]
			status = excel_data_df["Trạng thái"][idx]
			label = excel_data_df["Nhãn hiệu Máy phát điện"][idx]

			wattage = excel_data_df["Công suất Máy phát điện"][idx] if str(excel_data_df["Công suất Máy phát điện"][idx]) != "nan" else 0
			phase = excel_data_df["Phase"][idx] if str(excel_data_df["Phase"][idx]) != "nan" else '1P'
			sdate_use = excel_data_df["Ngày đưa vào sử dụng"][idx] if str(excel_data_df["Ngày đưa vào sử dụng"][idx]) != "nan" else None
			serial_engine = excel_data_df["Serial động cơ"][idx]
			serial_moto = excel_data_df["Serial đầu phát"][idx]
			cumulative_index = excel_data_df["Chỉ số vận hành lũy kế"][idx]
			product_code = excel_data_df["Product_code máy phát điện"][idx]
			insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
			fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None

			if ',' in str(wattage):
				wattage = float(str(wattage).replace(',','.'))
			dict_status = {
				'' : ' ',
				'ĐÃ ĐIỀU CHUYỂN' : 'changed',
				'HOẠT ĐỘNG TỐT' : 'good',
				'HỎNG' : 'error',
				'HỎNG HOÀN TOÀN' : 'error_full',
				'KHÔNG SỬ DỤNG' : 'not_used',
			}

			if sdate_use:
				sdate_use = datetime.strptime(sdate_use, '%d/%m/%Y')
			print(insurance_date, '========')
			if insurance_date:
				try:
					insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')
				except:
					insurance_date = datetime.strptime(insurance_date[3:], '%m/%Y')


			if fixing_date:
				fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

			author, created = MPDInfo.objects.get_or_create(
				name = name,
				code = code,
				parent_code = parent_code,
				parent_name = parent_name,
				qr_code = qr_code,
				site = site_id,
				wattage = wattage,
				label = label,
				status = dict_status.get(status, ''),
				fixing_date = fixing_date,
				phase = phase,
				insurance_date = insurance_date,
				serial_engine = serial_engine,
				product_code = product_code,
				sdate_use = sdate_use,
				serial_moto = serial_moto,
				cumulative_index = cumulative_index,
				)

			print('is created :', created)
	
	def _import_data_ats_info(excel_data_df):

		for idx, data in enumerate(excel_data_df["SITE_ID"]):
			try:
				print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
			except Exception as inst:
				print(inst)

			parent_code = excel_data_df["Mã đối tượng cha"][idx]
			mpd = MPDInfo.objects.filter(code = parent_code).first()
			code = excel_data_df["Mã đối tượng"][idx]
			name = excel_data_df["Tên đối tượng"][idx]
			qr_code = excel_data_df["Mã QR"][idx]
			status = excel_data_df["Trạng thái"][idx]
			label = excel_data_df["Nhãn hiệu ATS"][idx]
			insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
			fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None
			sdate_use = excel_data_df["Ngày đưa vào sử dụng"][idx] if str(excel_data_df["Ngày đưa vào sử dụng"][idx]) != "nan" else None
			customer_management = excel_data_df["Đơn vị chủ quản ATS"][idx] if str(excel_data_df["Đơn vị chủ quản ATS"][idx]) != "nan" else ''
			serial = excel_data_df["Serial ATS"][idx] if str(excel_data_df["Serial ATS"][idx]) != "nan" else ''
			type = excel_data_df["Loại TB ATS"][idx] if str(excel_data_df["Loại TB ATS"][idx]) != "nan" else ''
			product_code = excel_data_df["Product_code ATS"][idx] if str(excel_data_df["Product_code ATS"][idx]) != "nan" else ''


			dict_status = {
				'' : ' ',
				'ĐÃ ĐIỀU CHUYỂN' : 'changed',
				'HOẠT ĐỘNG TỐT' : 'good',
				'HỎNG' : 'error',
				'HỎNG HOÀN TOÀN' : 'error_full',
				'KHÔNG SỬ DỤNG' : 'not_used',
			}

			if sdate_use:
				sdate_use = datetime.strptime(sdate_use, '%d/%m/%Y')
			if insurance_date:
				try:
					insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')
				except:
					insurance_date = datetime.strptime(insurance_date[3:], '%m/%Y')


			if fixing_date:
				fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

			author, created = ATSInfo.objects.get_or_create(
				name = name,
				code = code,
				mpd = mpd,
				qr_code = qr_code,
				customer_management = customer_management,
				label = label,
				status = dict_status.get(status, ''),
				fixing_date = fixing_date,
				serial = serial,
				insurance_date = insurance_date,
				type = type,
				product_code = product_code,
				sdate_use = sdate_use,
				)

			print('is created :', created)
	
	def _import_data_cabinet(excel_data_df):
		list_site_not_found = []
		
		for idx, data in enumerate(excel_data_df["SITE_ID"]):
			try:
				print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
			except Exception as inst:
				print(inst)

			site_id = Site.objects.filter(id=data).first()

			if not site_id:
				print(' not found site ', data)
				list_site_not_found.append(data)
				continue

			parent_code = excel_data_df["Mã đối tượng cha"][idx]
			parent_name = excel_data_df["Tên đối tượng cha"][idx]
			code = excel_data_df["Mã đối tượng"][idx]
			name = excel_data_df["Tên đối tượng"][idx]
			qr_code = excel_data_df["Mã QR"][idx]
			status = excel_data_df["Trạng thái"][idx]
			label = excel_data_df["Nhãn hiệu Tủ nguồn"][idx]

			number_module_rectifier = excel_data_df["Số lượng khe rectifier"][idx] if str(excel_data_df["Số lượng khe rectifier"][idx]) != "nan" else 0
			number_rectifier = excel_data_df["Số lượng rectifier"][idx] if str(excel_data_df["Số lượng rectifier"][idx]) != "nan" else 0
			rectifier_power = excel_data_df["Công suất Rectifier (W)"][idx] if str(excel_data_df["Công suất Rectifier (W)"][idx]) != "nan" else 0
			sdate_use = excel_data_df["Ngày đưa vào sử dụng"][idx] if str(excel_data_df["Ngày đưa vào sử dụng"][idx]) != "nan" else None
			serial = excel_data_df["Serial tủ nguồn"][idx]
			device_conect = excel_data_df["Thiết bị đấu nối vào tủ nguồn"][idx]
			product_code = excel_data_df["Product_code tủ nguồn"][idx]
			insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
			fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None

			dict_status = {
				'' : ' ',
				'ĐÃ ĐIỀU CHUYỂN' : 'changed',
				'HOẠT ĐỘNG TỐT' : 'good',
				'HỎNG' : 'error',
				'HỎNG HOÀN TOÀN' : 'error_full',
				'KHÔNG SỬ DỤNG' : 'not_used',
			}

			if sdate_use:
				sdate_use = datetime.strptime(sdate_use, '%d/%m/%Y')

			if insurance_date:
				insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')

			if fixing_date:
				fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

			author, created = CabinetsSource.objects.get_or_create(
				name = name,
				code = code,
				parent_code = parent_code,
				parent_name = parent_name,
				qr_code = qr_code,
				site = site_id,
				number_module_rectifier = number_module_rectifier,
				label = label,
				status = dict_status.get(status, ''),
				fixing_date = fixing_date,
				number_rectifier = number_rectifier,
				insurance_date = insurance_date,
				rectifier_power = rectifier_power,
				serial = serial,
				product_code = product_code,
				sdate_use = sdate_use,
				device_conect = device_conect,)

			# CabinetsSource(
			# 	name = name,
			# 	code = code,
			# 	parent_code = parent_code,
			# 	parent_name = parent_name,
			# 	qr_code = qr_code,
			# 	site_id = site_id,
			# 	number_module_rectifier = number_module_rectifier,
			# 	label = label,
			# 	status = status,
			# 	fixing_date = fixing_date,
			# 	number_rectifier = number_rectifier,
			# 	insurance_date = insurance_date,
			# 	rectifier_power = rectifier_power,
			# 	serial = serial,
			# 	product_code = product_code,
			# 	sdate_use = sdate_use,
			# 	device_conect = device_conect,
			# )
			print('is created :', created)

	def _import_data_soh_info(excel_data_df):
		list_site_not_found = []
		for idx, data in enumerate(excel_data_df["SITE_ID"]):


			try:
				print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
			except Exception as inst:
				print(inst)

			site_id = Site.objects.filter(id=data).first()

			if not site_id:
				print(' not found site ', data)
				list_site_not_found.append(data)
				continue

			# site_id = data
			parent_code = excel_data_df["Mã đối tượng tủ nguồn"][idx]
			name = excel_data_df["Tên đối tượng"][idx]
			qr_code = excel_data_df["Mã QR"][idx]
			voltage = excel_data_df["Dòng tải"][idx] if str(excel_data_df["Dòng tải"][idx]) != "nan" else 0
			ird = excel_data_df["IRD"][idx] if str(excel_data_df["IRD"][idx]) != "nan" else 0
			irsx = excel_data_df["IRSX"][idx] if str(excel_data_df["IRSX"][idx]) != "nan" else 0
			ixa = excel_data_df["IXA"][idx] if str(excel_data_df["IXA"][idx]) != "nan" else 0
			timexa = excel_data_df["TIMEXA"][idx] if str(excel_data_df["TIMEXA"][idx]) != "nan" else 0
			ddm = excel_data_df["DDM"][idx] if str(excel_data_df["DDM"][idx]) != "nan" else 0
			time_backup = excel_data_df["Thời gian backup"][idx] if str(excel_data_df["Thời gian backup"][idx]) != "nan" else 0
			serial = excel_data_df["Serial tủ nguồn"][idx]	


			parent_code = CabinetsSource.objects.filter(code = parent_code).first()

			if not DataSOHInfo.objects.filter(site_id=site_id,cabinets_source=parent_code):

				author, created = DataSOHInfo.objects.get_or_create(
					name = name,
					cabinets_source = parent_code,
					qr_code = qr_code,
					voltage = voltage,
					ixa = ixa,
					ird = ird,
					irsx = irsx,
					timexa = timexa,
					ddm = ddm,
					time_backup = time_backup,
					serial = serial,
					site_id = site_id
				)
			else:
				obj_soh = DataSOHInfo.objects.get(site_id=site_id,cabinets_source=parent_code)
				obj_soh.name = name
				obj_soh.qr_code = qr_code
				obj_soh.voltage = voltage
				obj_soh.ixa = ixa
				obj_soh.ird = ird
				obj_soh.irsx = irsx
				obj_soh.timexa = timexa
				obj_soh.ddm = ddm
				obj_soh.time_backup = time_backup
				obj_soh.serial = serial
				obj_soh.save()

	if type_import == 'mpd_info':
		_import_data_mpd_info(excel_data_df)
	elif type_import == 'ats_info':
		_import_data_ats_info(excel_data_df)	
	elif type_import == 'cabinet':
		_import_data_cabinet(excel_data_df)	
	elif type_import == 'accu_info':
		_import_data_accu_info(excel_data_df)		
	elif type_import == 'soh_info':
		_import_data_soh_info(excel_data_df)	
					
	return HttpResponse(1)		

def del_log(request):
	log_id = request.POST["log_id"]

	accu_data_obj = ACCUData.objects.filter(log_id=log_id).delete()
	print(len(accu_data_obj),'accu_data_obj')
	log_obj = ACCUDataLog.objects.get(id=log_id).delete()
	print(log_obj,'log_obj')
	return HttpResponse(1)		


class IndexDataSOHConvert(TemplateView):
	template_name = 'apps/accu_dashboard/index-data-soh-convert.html'

	def _check_int_or_float(self,value):
  		# Kiểm tra nếu là chuỗi str thì return 0
		if isinstance(value, str) and str(value).strip() == "":
			return 0
		
		# Kiểm tra nếu là số float
		if int(value) == 0 and value != 0:
			return value
		
		# Kiểm tra nếu là số int
		if value and value%int(value) == 0:
			value = int(value)

		# Kiểm tra nếu 0.0 thì return 0
		if value == 0.0 or value == "0.0":
			value = 0

		return value

		
	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		data['current_start_time'] = (datetime.now() -timedelta(days=30)).strftime("%Y-%m-%d")
		
		data['current_end_time'] = datetime.now().strftime("%Y-%m-%d")
		
		list_data_site_soh = ACCUData.objects.all()

		list_data_soh = DataSOHInfo.objects.filter()

		dict_SOH = {}
		for i in list_data_site_soh:
			data_soh_site = list_data_soh.filter(site=i.site).first()
			_dtt_data = 0
			if data_soh_site:
				_dtt_data = data_soh_site.ixa*  data_soh_site.timexa
				print(_dtt_data)
				
			if dict_SOH.get(i.site):
				dict_SOH[i.site]['d_dm'] += i.number_of_jar * i.capacity
				dict_SOH[i.site]['d_tt'] += _dtt_data
				dict_SOH[i.site]['number_of_jar'] += i.number_of_jar
				dict_SOH[i.site]['backup_time'] += i.backup_time

			else:
				dict_SOH[i.site] = {}
				dict_SOH[i.site]['d_dm'] = i.number_of_jar * i.capacity
				dict_SOH[i.site]['d_tt'] = _dtt_data
				dict_SOH[i.site]['number_of_jar'] = i.number_of_jar
				dict_SOH[i.site]['backup_time'] = i.backup_time
				dict_SOH[i.site]['day_enter_active'] = i.day_enter_active

		for site in dict_SOH:
			dict_SOH[site]['SOH'] = round((dict_SOH[site]['d_tt'] / dict_SOH[site]['d_dm']) *100) if dict_SOH[site]['d_dm'] else 0

		list_data = []
		for key,value in dict_SOH.items():
			list_data.append({
				'site' : key,
				'number_of_jar' : value['number_of_jar'],
				'day_enter_active' : value['day_enter_active'],
				'd_dm' : value['d_dm'],
				'backup_time' : value['backup_time'],
				'd_tt' : self._check_int_or_float(round(value['d_tt'],2)),
				'SOH' : value['SOH'],
			})

		data['list_data_soh'] = list_data
		data['total'] = len(list_data)
		
		return data	


import re

def import_data_accu_info(request):

	path_file = "/Users/phantrung/dev/MOBIPHONE-new/data/TOACCU_MLMT.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')
	datas = ACCUInfo.objects.all()
	datas.delete()	

	for idx, data in enumerate(excel_data_df["SITE_ID"]):
		try:
			print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
		except Exception as inst:
			print(inst)
		
		# site_id = data
		parent_code = excel_data_df["Mã đối tượng cha"][idx]
		code = excel_data_df["Mã đối tượng"][idx]
		name = excel_data_df["Tên đối tượng"][idx]
		qr_code = excel_data_df["Mã QR"][idx]
		voltage = excel_data_df["Điện áp Accu"][idx] if str(excel_data_df["Điện áp Accu"][idx]) != "nan" else 0
		status = excel_data_df["Trạng thái"][idx] if str(excel_data_df["Trạng thái"][idx]) != "nan" else ''
		product_code = excel_data_df["Product_code ACCU"][idx]
		label = excel_data_df["Nhãn hiệu Accu"][idx]
		battery_capacity = excel_data_df["Dung lượng bình Accu"][idx] if str(excel_data_df["Dung lượng bình Accu"][idx]) != "nan" else 0
		fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None
		number_accu = excel_data_df["Số lượng Bình Accu"][idx]  if str(excel_data_df["Số lượng Bình Accu"][idx]) != "nan" else 0
		insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
		time_backup = excel_data_df["Thời gian Backup"][idx] if str(excel_data_df["Thời gian Backup"][idx]) != "nan" else 0
		serial = excel_data_df["Serial ACCU"][idx]	

		battery_capacity = re.findall(r'\d+', str(battery_capacity))
		battery_capacity = battery_capacity[0] if battery_capacity else 0

		parent_code = CabinetsSource.objects.filter(code = parent_code).first()

		dict_status = {
			'' : ' ',
			'ĐÃ ĐIỀU CHUYỂN' : 'changed',
			'HOẠT ĐỘNG TỐT' : 'good',
			'HỎNG' : 'error',
			'HỎNG HOÀN TOÀN' : 'error_full',
			'KHÔNG SỬ DỤNG' : 'not_used',
		}

		if insurance_date:
			insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')

		if fixing_date:
			fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

		print(dict_status.get(status), status)
		author, created = ACCUInfo.objects.get_or_create(
			name = name,
			code = code,
			cabinets_source = parent_code,
			qr_code = qr_code,
			voltage = voltage,
			battery_capacity = battery_capacity,
			label = label,
			status = dict_status.get(status),
			fixing_date = fixing_date,
			number_accu = number_accu,
			insurance_date = insurance_date,
			time_backup = time_backup,
			serial = serial,
			product_code = product_code,
		)

	datas = ACCUInfo.objects.all()
	print('truturtrtrtrt', datas.count())
		
	return HttpResponse(1)




def import_data_source_info(request):
	path_file = "/Users/phantrung/dev/MOBIPHONE-new/data/TUNGUON_MLMT.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')
	list_site_not_found = []
	datas = CabinetsSource.objects.all()
	print(len(datas))
	datas.delete()

	for idx, data in enumerate(excel_data_df["SITE_ID"]):
		try:
			print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
		except Exception as inst:
			print(inst)

		site_id = Site.objects.filter(id=data).first()

		if not site_id:
			print(' not found site ', data)
			list_site_not_found.append(data)
			continue

		parent_code = excel_data_df["Mã đối tượng cha"][idx]
		parent_name = excel_data_df["Tên đối tượng cha"][idx]
		code = excel_data_df["Mã đối tượng"][idx]
		name = excel_data_df["Tên đối tượng"][idx]
		qr_code = excel_data_df["Mã QR"][idx]
		status = excel_data_df["Trạng thái"][idx]
		label = excel_data_df["Nhãn hiệu Tủ nguồn"][idx]

		number_module_rectifier = excel_data_df["Số lượng khe rectifier"][idx] if str(excel_data_df["Số lượng khe rectifier"][idx]) != "nan" else 0
		number_rectifier = excel_data_df["Số lượng rectifier"][idx] if str(excel_data_df["Số lượng rectifier"][idx]) != "nan" else 0
		rectifier_power = excel_data_df["Công suất Rectifier (W)"][idx] if str(excel_data_df["Công suất Rectifier (W)"][idx]) != "nan" else 0
		sdate_use = excel_data_df["Ngày đưa vào sử dụng"][idx] if str(excel_data_df["Ngày đưa vào sử dụng"][idx]) != "nan" else None
		serial = excel_data_df["Serial tủ nguồn"][idx]
		device_conect = excel_data_df["Thiết bị đấu nối vào tủ nguồn"][idx]
		product_code = excel_data_df["Product_code tủ nguồn"][idx]
		insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
		fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None

		dict_status = {
			'' : ' ',
			'ĐÃ ĐIỀU CHUYỂN' : 'changed',
			'HOẠT ĐỘNG TỐT' : 'good',
			'HỎNG' : 'error',
			'HỎNG HOÀN TOÀN' : 'error_full',
			'KHÔNG SỬ DỤNG' : 'not_used',
		}

		if sdate_use:
			sdate_use = datetime.strptime(sdate_use, '%d/%m/%Y')

		if insurance_date:
			insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')

		if fixing_date:
			fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

		author, created = CabinetsSource.objects.get_or_create(
			name = name,
			code = code,
			parent_code = parent_code,
			parent_name = parent_name,
			qr_code = qr_code,
			site = site_id,
			number_module_rectifier = number_module_rectifier,
			label = label,
			status = dict_status.get(status, ''),
			fixing_date = fixing_date,
			number_rectifier = number_rectifier,
			insurance_date = insurance_date,
			rectifier_power = rectifier_power,
			serial = serial,
			product_code = product_code,
			sdate_use = sdate_use,
			device_conect = device_conect,)

		# CabinetsSource(
		# 	name = name,
		# 	code = code,
		# 	parent_code = parent_code,
		# 	parent_name = parent_name,
		# 	qr_code = qr_code,
		# 	site_id = site_id,
		# 	number_module_rectifier = number_module_rectifier,
		# 	label = label,
		# 	status = status,
		# 	fixing_date = fixing_date,
		# 	number_rectifier = number_rectifier,
		# 	insurance_date = insurance_date,
		# 	rectifier_power = rectifier_power,
		# 	serial = serial,
		# 	product_code = product_code,
		# 	sdate_use = sdate_use,
		# 	device_conect = device_conect,
		# )
		print('is created :', created)

	# dave not found data
	return HttpResponse(json.dumps(list_site_not_found))



def import_data_ats_info(request):
	path_file = "/Users/phantrung/dev/MOBIPHONE-new/data/ATS_MLMT.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')
	list_site_not_found = []
	datas = ATSInfo.objects.all()
	# print(len(datas))
	# return 1
	datas.delete()

	for idx, data in enumerate(excel_data_df["SITE_ID"]):
		try:
			print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
		except Exception as inst:
			print(inst)

		parent_code = excel_data_df["Mã đối tượng cha"][idx]
		mpd = MPDInfo.objects.filter(code = parent_code).first()
		code = excel_data_df["Mã đối tượng"][idx]
		name = excel_data_df["Tên đối tượng"][idx]
		qr_code = excel_data_df["Mã QR"][idx]
		status = excel_data_df["Trạng thái"][idx]
		label = excel_data_df["Nhãn hiệu ATS"][idx]
		insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
		fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None
		sdate_use = excel_data_df["Ngày đưa vào sử dụng"][idx] if str(excel_data_df["Ngày đưa vào sử dụng"][idx]) != "nan" else None
		customer_management = excel_data_df["Đơn vị chủ quản ATS"][idx] if str(excel_data_df["Đơn vị chủ quản ATS"][idx]) != "nan" else ''
		serial = excel_data_df["Serial ATS"][idx] if str(excel_data_df["Serial ATS"][idx]) != "nan" else ''
		type = excel_data_df["Loại TB ATS"][idx] if str(excel_data_df["Loại TB ATS"][idx]) != "nan" else ''
		product_code = excel_data_df["Product_code ATS"][idx] if str(excel_data_df["Product_code ATS"][idx]) != "nan" else ''


		dict_status = {
			'' : ' ',
			'ĐÃ ĐIỀU CHUYỂN' : 'changed',
			'HOẠT ĐỘNG TỐT' : 'good',
			'HỎNG' : 'error',
			'HỎNG HOÀN TOÀN' : 'error_full',
			'KHÔNG SỬ DỤNG' : 'not_used',
		}

		if sdate_use:
			sdate_use = datetime.strptime(sdate_use, '%d/%m/%Y')
		if insurance_date:
			try:
				insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')
			except:
				insurance_date = datetime.strptime(insurance_date[3:], '%m/%Y')


		if fixing_date:
			fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

		author, created = ATSInfo.objects.get_or_create(
			name = name,
			code = code,
			mpd = mpd,
			qr_code = qr_code,
			customer_management = customer_management,
			label = label,
			status = dict_status.get(status, ''),
			fixing_date = fixing_date,
			serial = serial,
			insurance_date = insurance_date,
			type = type,
			product_code = product_code,
			sdate_use = sdate_use,
			)

		print('is created :', created)
	
	datas = ATSInfo.objects.all()
	print(len(datas))

	# dave not found data
	return HttpResponse(json.dumps(list_site_not_found))


def import_data_mpd_info(request):
	path_file = "/Users/phantrung/dev/MOBIPHONE-new/data/MPD_MLMT.xlsx"
	excel_data_df = pd.read_excel(path_file, engine='openpyxl')
	list_site_not_found = []
	datas = MPDInfo.objects.all()
	# print(len(datas))
	# return 1
	datas.delete()

	for idx, data in enumerate(excel_data_df["SITE_ID"]):
		try:
			print(round(idx/len(excel_data_df["SITE_ID"]),2 )*100, "-------%---------")
		except Exception as inst:
			print(inst)

		site_id = Site.objects.filter(id=data).first()

		if not site_id:
			print(' not found site ', data)
			list_site_not_found.append(data)
			continue

		parent_code = excel_data_df["Mã đối tượng cha"][idx]
		parent_name = excel_data_df["Tên đối tượng cha"][idx]
		code = excel_data_df["Mã đối tượng"][idx]
		name = excel_data_df["Tên đối tượng"][idx]
		qr_code = excel_data_df["Mã QR"][idx]
		status = excel_data_df["Trạng thái"][idx]
		label = excel_data_df["Nhãn hiệu Máy phát điện"][idx]

		wattage = excel_data_df["Công suất Máy phát điện"][idx] if str(excel_data_df["Công suất Máy phát điện"][idx]) != "nan" else 0
		phase = excel_data_df["Phase"][idx] if str(excel_data_df["Phase"][idx]) != "nan" else '1P'
		sdate_use = excel_data_df["Ngày đưa vào sử dụng"][idx] if str(excel_data_df["Ngày đưa vào sử dụng"][idx]) != "nan" else None
		serial_engine = excel_data_df["Serial động cơ"][idx]
		serial_moto = excel_data_df["Serial đầu phát"][idx]
		cumulative_index = excel_data_df["Chỉ số vận hành lũy kế"][idx]
		product_code = excel_data_df["Product_code máy phát điện"][idx]
		insurance_date = excel_data_df["Thời hạn bảo hành"][idx] if str(excel_data_df["Thời hạn bảo hành"][idx]) != "nan" else None
		fixing_date = excel_data_df["Thời hạn bảo dưỡng"][idx] if str(excel_data_df["Thời hạn bảo dưỡng"][idx]) != "nan" else None

		if ',' in str(wattage):
			wattage = float(str(wattage).replace(',','.'))
		dict_status = {
			'' : ' ',
			'ĐÃ ĐIỀU CHUYỂN' : 'changed',
			'HOẠT ĐỘNG TỐT' : 'good',
			'HỎNG' : 'error',
			'HỎNG HOÀN TOÀN' : 'error_full',
			'KHÔNG SỬ DỤNG' : 'not_used',
		}

		if sdate_use:
			sdate_use = datetime.strptime(sdate_use, '%d/%m/%Y')
		print(insurance_date, '========')
		if insurance_date:
			try:
				insurance_date = datetime.strptime(insurance_date, '%d/%m/%Y')
			except:
				insurance_date = datetime.strptime(insurance_date[3:], '%m/%Y')


		if fixing_date:
			fixing_date = datetime.strptime(fixing_date, '%d/%m/%Y')

		author, created = MPDInfo.objects.get_or_create(
			name = name,
			code = code,
			parent_code = parent_code,
			parent_name = parent_name,
			qr_code = qr_code,
			site = site_id,
			wattage = wattage,
			label = label,
			status = dict_status.get(status, ''),
			fixing_date = fixing_date,
			phase = phase,
			insurance_date = insurance_date,
			serial_engine = serial_engine,
			product_code = product_code,
			sdate_use = sdate_use,
			serial_moto = serial_moto,
			cumulative_index = cumulative_index,
			)

		print('is created :', created)
	
	datas = MPDInfo.objects.all()
	print(len(datas))

	# dave not found data
	return HttpResponse(json.dumps(list_site_not_found))


class MPDIndex(TemplateView):
	template_name = 'apps/accu_dashboard/index-mpd-info.html'
		
	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		
		data['link_example'] = '/static/csv/mpd_info_example.xlsx'
		data['type_import'] = 'mpd_info'
		return data	

def get_data_mpd_info(request):
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	data_mpd_info = MPDInfo().get_data_mpd_info(param_page,param_page_size)
	data_total = MPDInfo().get_data_mpd_info_export()
	
	list_data_table = data_mpd_info
	data = {
		'list_data_mpd_info':list_data_table,
		'total' : len(data_total)
	}

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/table-mpd-info.html'
	return render(request, tmp_file_path, data)

def load_update_mpd_info(request):
	param_mpd_id = request.POST["mpd_id"]

	data_mpd_info = MPDInfo.objects.get(id=param_mpd_id)
	data_mpd_info.sdate_use = data_mpd_info.sdate_use.strftime('%Y-%m-%dT%H:%M')
	data_mpd_info.insurance_date = data_mpd_info.insurance_date.strftime('%Y-%m-%dT%H:%M')
	data_mpd_info.fixing_date = data_mpd_info.fixing_date.strftime('%Y-%m-%dT%H:%M')
	data = {
		'data_mpd_info':data_mpd_info
	}
	list_status = [
		{'id':'changed','name':'ĐÃ ĐIỀU CHUYỂN'},
		{'id':'good','name':'HOẠT ĐỘNG TỐT'},
		{'id':'error','name':'HỎNG'},
		{'id':'error_full','name':'HỎNG HOÀN TOÀN'},
		{'id':'not_used','name':'KHÔNG SỬ DỤNG'},
	]

	list_site = Site.objects.all()
	list_site = [ {'id' : item.id,'name':item.pmt_code} for item in list_site ]

	data['list_status'] = list_status
	data['list_site'] = list_site

	tmp_file_path = 'apps/accu_dashboard/modal/modal-update-mpd-info-detail.html'

	return render(request, tmp_file_path, data)

def update_mpd_info(request):
	
	param_mpd_id = request.POST["mpd_id"]
	param_name = request.POST["name"]
	param_code = request.POST["code"]
	param_product_code = request.POST["product_code"]
	param_qr_code = request.POST["qr_code"]
	param_wattage = request.POST["wattage"]
	param_phase = request.POST["phase"]
	param_site = request.POST["site"]
	param_parent_code = request.POST["parent_code"]
	param_parent_name = request.POST["parent_name"]
	param_sdate_use = request.POST["sdate_use"]
	param_insurance_date = request.POST["insurance_date"]
	param_fixing_date = request.POST["fixing_date"]
	param_serial_engine = request.POST["serial_engine"]
	param_serial_moto = request.POST["serial_moto"]
	param_label = request.POST["label"]
	param_status = request.POST["status"]
	param_cumulative_index = request.POST["cumulative_index"]

	obj_mpd_info = MPDInfo.objects.get(id=param_mpd_id)
	obj_mpd_info.name = param_name
	obj_mpd_info.code = param_code
	obj_mpd_info.product_code = param_product_code
	obj_mpd_info.qr_code = param_qr_code
	obj_mpd_info.wattage = param_wattage
	obj_mpd_info.phase = param_phase
	obj_mpd_info.site_id = param_site
	obj_mpd_info.parent_code = param_parent_code
	obj_mpd_info.parent_name = param_parent_name
	obj_mpd_info.sdate_use = datetime.strptime(param_sdate_use, '%Y-%m-%dT%H:%M')
	obj_mpd_info.insurance_date = datetime.strptime(param_insurance_date, '%Y-%m-%dT%H:%M')
	obj_mpd_info.fixing_date = datetime.strptime(param_fixing_date, '%Y-%m-%dT%H:%M')
	obj_mpd_info.serial_engine = param_serial_engine
	obj_mpd_info.serial_moto = param_serial_moto
	obj_mpd_info.label = param_label
	obj_mpd_info.status = param_status
	obj_mpd_info.cumulative_index = param_cumulative_index
	obj_mpd_info.save()

	return HttpResponse(1)


class MPDInfoDeleteView(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
  model = MPDInfo
  permission_required = []
  template_name = 'apps/accu_dashboard/modal/mpd_info_delete_detail.html'
  # success_url = reverse_lazy('site_setting_property')
  success_message = 'Delete successed'

  def get_context_data(self, **kwargs):
    data = super().get_context_data()
    
    mpd_id = self.kwargs['pk']

    obj_mpd = MPDInfo.objects.get(pk=mpd_id)

    data["obj_mpd"] = obj_mpd

    return data
    
  def delete(self, request, *args, **kwargs):
    messages.success(self.request, self.success_message)
    
    obj_mpd_info = MPDInfo.objects.filter(id=self.kwargs['pk'])[0]
    obj_mpd_info.delete()

    return self.get_success_url()

  def get_success_url(self):
    
    return redirect("/mpd-index")


class ATSIndex(TemplateView):
	template_name = 'apps/accu_dashboard/index-ats-info.html'
		
	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		
		data['link_example'] = '/static/csv/ats_info_example.xlsx'
		data['type_import'] = 'ats_info'
		return data	

def get_data_ats_info(request):
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	data_ats_info = ATSInfo().get_data_ats_info(param_page,param_page_size)
	data_total = ATSInfo().get_data_ats_info_export()
	
	list_data_table = data_ats_info
	data = {
		'list_data_ats_info':list_data_table,
		'total' : len(data_total)
	}

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/table-ats-info.html'
	return render(request, tmp_file_path, data)

def load_update_ats_info(request):
	param_ats_id = request.POST["ats_id"]

	data_ats_info = ATSInfo.objects.get(id=param_ats_id)
	data_ats_info.sdate_use = data_ats_info.sdate_use.strftime('%Y-%m-%dT%H:%M')
	data_ats_info.insurance_date = data_ats_info.insurance_date.strftime('%Y-%m-%dT%H:%M') if data_ats_info.insurance_date else datetime.now().strftime('%Y-%m-%dT%H:%M') 
	data_ats_info.fixing_date = data_ats_info.fixing_date.strftime('%Y-%m-%dT%H:%M')
	data = {
		'data_ats_info':data_ats_info
	}
	list_status = [
		{'id':'changed','name':'ĐÃ ĐIỀU CHUYỂN'},
		{'id':'good','name':'HOẠT ĐỘNG TỐT'},
		{'id':'error','name':'HỎNG'},
		{'id':'error_full','name':'HỎNG HOÀN TOÀN'},
		{'id':'not_used','name':'KHÔNG SỬ DỤNG'},
	]

	list_mpd = MPDInfo.objects.all()
	list_mpd = [ {'id' : item.id,'name':item.name + f'({item.code})'} for item in list_mpd ]

	data['list_status'] = list_status
	data['list_mpd'] = list_mpd

	tmp_file_path = 'apps/accu_dashboard/modal/modal-update-ats-info-detail.html'

	return render(request, tmp_file_path, data)

def update_ats_info(request):
	param_ats_id = request.POST["ats_id"]
	param_name = request.POST["name"]
	param_code = request.POST["code"]
	param_product_code = request.POST["product_code"]
	param_qr_code = request.POST["qr_code"]
	param_customer_management = request.POST["customer_management"]
	param_mpd = request.POST["mpd"]
	param_sdate_use = request.POST["sdate_use"]
	param_insurance_date = request.POST["insurance_date"]
	param_fixing_date = request.POST["fixing_date"]
	param_serial = request.POST["serial"]
	param_type = request.POST["type"]
	param_label = request.POST["label"]
	param_status = request.POST["status"]

	obj_ats_info = ATSInfo.objects.get(id=param_ats_id)
	obj_ats_info.name = param_name
	obj_ats_info.code = param_code
	obj_ats_info.product_code = param_product_code
	obj_ats_info.qr_code = param_qr_code
	obj_ats_info.customer_management = param_customer_management
	obj_ats_info.mpd_id = param_mpd
	obj_ats_info.sdate_use = datetime.strptime(param_sdate_use, '%Y-%m-%dT%H:%M')
	obj_ats_info.insurance_date = datetime.strptime(param_insurance_date, '%Y-%m-%dT%H:%M')
	obj_ats_info.fixing_date = datetime.strptime(param_fixing_date, '%Y-%m-%dT%H:%M')
	obj_ats_info.serial = param_serial
	obj_ats_info.type = param_type
	obj_ats_info.label = param_label
	obj_ats_info.status = param_status
	obj_ats_info.save()

	return HttpResponse(1)


class ATSInfoDeleteView(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
  model = ATSInfo
  permission_required = []
  template_name = 'apps/accu_dashboard/modal/ats_info_delete_detail.html'
  # success_url = reverse_lazy('site_setting_property')
  success_message = 'Delete successed'

  def get_context_data(self, **kwargs):
    data = super().get_context_data()
    
    ats_id = self.kwargs['pk']

    obj_ats = ATSInfo.objects.get(pk=ats_id)

    data["obj_ats"] = obj_ats

    return data
    
  def delete(self, request, *args, **kwargs):
    messages.success(self.request, self.success_message)
    
    obj_ats_info = ATSInfo.objects.filter(id=self.kwargs['pk'])[0]
    obj_ats_info.delete()

    return self.get_success_url()

  def get_success_url(self):
    
    return redirect("/ats-index")


class CabinetIndex(TemplateView):
	template_name = 'apps/accu_dashboard/index-cabinet.html'
		
	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		
		data['link_example'] = '/static/csv/cabinet_example.xlsx'
		data['type_import'] = 'cabinet'
		return data	

def get_data_cabinets(request):
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	data_cabinet_info = CabinetsSource().get_data_cabinets_source_info(param_page,param_page_size)
	data_total = CabinetsSource().get_data_cabinets_source_info_export()
	
	list_data_table = data_cabinet_info
	data = {
		'list_data_cabinet':list_data_table,
		'total' : len(data_total)
	}

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/table-cabinet.html'
	return render(request, tmp_file_path, data)

def load_update_cabinets(request):
	param_cabinet_id = request.POST["cabinet_id"]

	data_cabinet_info = CabinetsSource.objects.get(id=param_cabinet_id)
	data_cabinet_info.sdate_use = data_cabinet_info.sdate_use.strftime('%Y-%m-%dT%H:%M')
	data_cabinet_info.insurance_date = data_cabinet_info.insurance_date.strftime('%Y-%m-%dT%H:%M')
	data_cabinet_info.fixing_date = data_cabinet_info.fixing_date.strftime('%Y-%m-%dT%H:%M')
	data = {
		'data_cabinet_info':data_cabinet_info
	}
	list_status = [
		{'id':'changed','name':'ĐÃ ĐIỀU CHUYỂN'},
		{'id':'good','name':'HOẠT ĐỘNG TỐT'},
		{'id':'error','name':'HỎNG'},
		{'id':'error_full','name':'HỎNG HOÀN TOÀN'},
		{'id':'not_used','name':'KHÔNG SỬ DỤNG'},
	]

	list_site = Site.objects.all()
	list_site = [ {'id' : item.id,'name':item.pmt_code} for item in list_site ]

	data['list_status'] = list_status
	data['list_site'] = list_site

	tmp_file_path = 'apps/accu_dashboard/modal/modal-update-cabinet-detail.html'

	return render(request, tmp_file_path, data)

def update_cabinets(request):
	
	param_cabinet_id = request.POST["cabinet_id"]
	param_name = request.POST["name"]
	param_code = request.POST["code"]
	param_product_code = request.POST["product_code"]
	param_qr_code = request.POST["qr_code"]
	param_parent_code = request.POST["parent_code"]
	param_parent_name = request.POST["parent_name"]
	param_site = request.POST["site"]
	param_sdate_use = request.POST["sdate_use"]
	param_insurance_date = request.POST["insurance_date"]
	param_fixing_date = request.POST["fixing_date"]
	param_serial = request.POST["serial"]
	param_label = request.POST["label"]
	param_status = request.POST["status"]
	param_number_rectifier = request.POST["number_rectifier"]
	param_number_module_rectifier = request.POST["number_module_rectifier"]
	param_rectifier_power = request.POST["rectifier_power"]

	obj_ats_info = CabinetsSource.objects.get(id=param_cabinet_id)
	obj_ats_info.name = param_name
	obj_ats_info.code = param_code
	obj_ats_info.product_code = param_product_code
	obj_ats_info.qr_code = param_qr_code
	obj_ats_info.parent_code = param_parent_code
	obj_ats_info.site_id = param_site
	obj_ats_info.sdate_use = datetime.strptime(param_sdate_use, '%Y-%m-%dT%H:%M')
	obj_ats_info.insurance_date = datetime.strptime(param_insurance_date, '%Y-%m-%dT%H:%M')
	obj_ats_info.fixing_date = datetime.strptime(param_fixing_date, '%Y-%m-%dT%H:%M')
	obj_ats_info.serial = param_serial
	obj_ats_info.parent_name = param_parent_name
	obj_ats_info.label = param_label
	obj_ats_info.status = param_status
	obj_ats_info.number_rectifier = param_number_rectifier
	obj_ats_info.number_module_rectifier = param_number_module_rectifier
	obj_ats_info.rectifier_power = param_rectifier_power
	obj_ats_info.save()

	return HttpResponse(1)


class CabinetDeleteView(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
  model = CabinetsSource
  permission_required = []
  template_name = 'apps/accu_dashboard/modal/cabinet_delete_detail.html'
  # success_url = reverse_lazy('site_setting_property')
  success_message = 'Delete successed'

  def get_context_data(self, **kwargs):
    data = super().get_context_data()
    
    cabinet_id = self.kwargs['pk']

    obj_cabinet = CabinetsSource.objects.get(pk=cabinet_id)

    data["obj_cabinet"] = obj_cabinet

    return data
    
  def delete(self, request, *args, **kwargs):
    messages.success(self.request, self.success_message)
    
    obj_cabinet_info = CabinetsSource.objects.filter(id=self.kwargs['pk'])[0]
    obj_cabinet_info.delete()

    return self.get_success_url()

  def get_success_url(self):
    
    return redirect("/cabinet-index")


class ACCUIndex(TemplateView):
	template_name = 'apps/accu_dashboard/index-accu-info.html'
		
	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		
		data['link_example'] = '/static/csv/accu_info_example.xlsx'
		data['type_import'] = 'accu_info'
		return data	

def get_data_accu_info(request):
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	data_accu_info = ACCUInfo().get_data_accu_info(param_page,param_page_size)
	data_total = ACCUInfo().get_data_accu_info_export()
	
	list_data_table = data_accu_info
	data = {
		'list_data_accu_info':list_data_table,
		'total' : len(data_total)
	}

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/table-accu-info.html'
	return render(request, tmp_file_path, data)

def load_update_accu_info(request):
	param_accu_id = request.POST["accu_id"]

	data_accu_info = ACCUInfo.objects.get(id=param_accu_id)
	data_accu_info.sdate_use = data_accu_info.sdate_use.strftime('%Y-%m-%dT%H:%M') if data_accu_info.sdate_use else datetime.now().strftime('%Y-%m-%dT%H:%M')
	data_accu_info.insurance_date = data_accu_info.insurance_date.strftime('%Y-%m-%dT%H:%M')
	data_accu_info.fixing_date = data_accu_info.fixing_date.strftime('%Y-%m-%dT%H:%M')
	data = {
		'data_accu_info':data_accu_info
	}
	list_status = [
		{'id':'changed','name':'ĐÃ ĐIỀU CHUYỂN'},
		{'id':'good','name':'HOẠT ĐỘNG TỐT'},
		{'id':'error','name':'HỎNG'},
		{'id':'error_full','name':'HỎNG HOÀN TOÀN'},
		{'id':'not_used','name':'KHÔNG SỬ DỤNG'},
	]

	list_cabinet = CabinetsSource.objects.all()
	list_cabinet = [ {'id' : item.id,'name':item.name + f'({item.code})'} for item in list_cabinet ]
	print(len(list_cabinet))
	data['list_status'] = list_status
	data['list_cabinet'] = list_cabinet

	tmp_file_path = 'apps/accu_dashboard/modal/modal-update-accu-info-detail.html'

	return render(request, tmp_file_path, data)

def update_accu_info(request):
	
	param_accu_id = request.POST["accu_id"]
	param_name = request.POST["name"]
	param_code = request.POST["code"]
	param_product_code = request.POST["product_code"]
	param_qr_code = request.POST["qr_code"]
	param_voltage = request.POST["voltage"]
	param_cabinet = request.POST["cabinet"]
	param_sdate_use = request.POST["sdate_use"]
	param_insurance_date = request.POST["insurance_date"]
	param_fixing_date = request.POST["fixing_date"]
	param_serial = request.POST["serial"]
	param_label = request.POST["label"]
	param_battery_capacity = request.POST["battery_capacity"]
	param_number_accu = request.POST["number_accu"]
	param_time_backup = request.POST["time_backup"]
	param_status = request.POST["status"]

	obj_accu_info = ACCUInfo.objects.get(id=param_accu_id)
	obj_accu_info.name = param_name
	obj_accu_info.code = param_code
	obj_accu_info.product_code = param_product_code
	obj_accu_info.qr_code = param_qr_code
	obj_accu_info.voltage = param_voltage
	obj_accu_info.cabinets_source_id = param_cabinet
	obj_accu_info.sdate_use = datetime.strptime(param_sdate_use, '%Y-%m-%dT%H:%M')
	obj_accu_info.insurance_date = datetime.strptime(param_insurance_date, '%Y-%m-%dT%H:%M')
	obj_accu_info.fixing_date = datetime.strptime(param_fixing_date, '%Y-%m-%dT%H:%M')
	obj_accu_info.serial = param_serial
	obj_accu_info.time_backup = param_time_backup
	obj_accu_info.battery_capacity = param_battery_capacity
	obj_accu_info.label = param_label
	obj_accu_info.number_accu = param_number_accu
	obj_accu_info.status = param_status
	obj_accu_info.save()

	return HttpResponse(1)


class ACCUInfoDeleteView(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
  model = ACCUInfo
  permission_required = []
  template_name = 'apps/accu_dashboard/modal/accu_info_delete_detail.html'
  # success_url = reverse_lazy('site_setting_property')
  success_message = 'Delete successed'

  def get_context_data(self, **kwargs):
    data = super().get_context_data()
    
    accu_id = self.kwargs['pk']

    obj_accu = ACCUInfo.objects.get(pk=accu_id)

    data["obj_accu"] = obj_accu

    return data
    
  def delete(self, request, *args, **kwargs):
    messages.success(self.request, self.success_message)
    
    obj_accu_info = ACCUInfo.objects.filter(id=self.kwargs['pk'])[0]
    obj_accu_info.delete()

    return self.get_success_url()

  def get_success_url(self):
    
    return redirect("/accu-index")


class DataSOHIndex(TemplateView):
	template_name = 'apps/accu_dashboard/index-soh-info.html'
		
	def get_context_data(self, *args, **kwargs):
		data = super().get_context_data()
		
		data['link_example'] = '/static/csv/data_soh_example.xlsx'
		data['type_import'] = 'soh_info'
		return data	

def get_data_soh_info(request):
	param_page = request.POST["page"]
	param_page_size = request.POST["page_size"]

	data_soh_info = DataSOHInfo().get_data_soh_info(param_page,param_page_size)
	data_total = DataSOHInfo().get_data_soh_info_export()
	
	list_data_table = data_soh_info
	data = {
		'list_data_soh_info':list_data_table,
		'total' : len(data_total)
	}

	tmp_file_path = 'apps/accu_dashboard/detail-dashboard/table-soh-info.html'
	return render(request, tmp_file_path, data)

class DataSOHInfoDeleteView(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
  model = DataSOHInfo
  permission_required = []
  template_name = 'apps/accu_dashboard/modal/soh_info_delete_detail.html'
  # success_url = reverse_lazy('site_setting_property')
  success_message = 'Delete successed'

  def get_context_data(self, **kwargs):
    data = super().get_context_data()
    
    soh_id = self.kwargs['pk']

    obj_soh = DataSOHInfo.objects.get(pk=soh_id)

    data["obj_soh"] = obj_soh

    return data
    
  def delete(self, request, *args, **kwargs):
    messages.success(self.request, self.success_message)
    
    obj_soh_info = DataSOHInfo.objects.filter(id=self.kwargs['pk'])[0]
    obj_soh_info.delete()

    return self.get_success_url()

  def get_success_url(self):
    
    return redirect("/soh-index")