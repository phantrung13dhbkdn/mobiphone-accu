
def get_data_excel_summary(list_data_sumary):

  dict_data = {
    "No." : [],
    "SITE_ID" : [],
    "Start Time" : [],
    "End Time" : [],
    "Time MLL" : [],
    "PROVINCE" : [],
    "DISTRICT" : [],
    "GROUP" : [],
    "NETWORk" : [],
  }
  
  index = 0
  for summary in list_data_sumary:
    index += 1
    dict_data["No."].append(index)
    dict_data["SITE_ID"].append(summary.site)
    dict_data["Start Time"].append(summary.sdate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["End Time"].append(summary.edate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["Time MLL"].append(summary.durations)
    dict_data["PROVINCE"].append(summary.provice)
    dict_data["DISTRICT"].append(summary.district)
    dict_data["GROUP"].append(summary.groups)
    dict_data["NETWORk"].append(summary.network)

  return dict_data
  
def get_data_excel_summary_md(list_data_sumary):

  dict_data = {
    "No." : [],
    "SITE_ID" : [],
    "Start Time" : [],
    "End Time" : [],
    "Time MD" : [],
    "PROVINCE" : [],
    "DISTRICT" : [],
    "GROUP" : [],
    "NETWORk" : [],
    "TYPE" : [],
  }
  
  index = 0
  for summary in list_data_sumary:
    index += 1
    dict_data["No."].append(index)
    dict_data["SITE_ID"].append(summary.site)
    dict_data["Start Time"].append(summary.sdate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["End Time"].append(summary.edate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["Time MD"].append(summary.durations)
    dict_data["PROVINCE"].append(summary.provice)
    dict_data["DISTRICT"].append(summary.district)
    dict_data["GROUP"].append(summary.groups)
    dict_data["NETWORk"].append(summary.network)
    dict_data["TYPE"].append(summary.alarm_type)
  
  return dict_data 

def get_data_excel_summary_mpd(list_data_sumary):

  dict_data = {
    "No." : [],
    "SITE_ID" : [],
    "Start Time" : [],
    "End Time" : [],
    "Time MD" : [],
    "PROVINCE" : [],
    "DISTRICT" : [],
    "GROUP" : [],
    "NETWORk" : [],
    "TYPE" : [],
  }
  
  index = 0
  for summary in list_data_sumary:
    index += 1
    dict_data["No."].append(index)
    dict_data["SITE_ID"].append(summary.site)
    dict_data["Start Time"].append(summary.sdate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["End Time"].append(summary.edate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["Time MD"].append(summary.durations)
    dict_data["PROVINCE"].append(summary.provice)
    dict_data["DISTRICT"].append(summary.district)
    dict_data["GROUP"].append(summary.groups)
    dict_data["NETWORk"].append(summary.network)
    dict_data["TYPE"].append(summary.alarm_type)
  
  return dict_data 
 
def get_data_excel_summary_accu(list_data_sumary):

  dict_data = {
    "No." : [],
    "SITE_ID" : [],
    "Start Time" : [],
    "End Time" : [],
    "Time MD" : [],
    "PROVINCE" : [],
    "DISTRICT" : [],
    "GROUP" : [],
    "NETWORk" : [],
  }
  
  index = 0
  for summary in list_data_sumary:
    index += 1
    dict_data["No."].append(index)
    dict_data["SITE_ID"].append(summary.site)
    dict_data["Start Time"].append(summary.sdate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["End Time"].append(summary.edate.strftime('%Y-%m-%d %H:%M:%S'))
    dict_data["Time MD"].append(summary.durations)
    dict_data["PROVINCE"].append(summary.provice)
    dict_data["DISTRICT"].append(summary.district)
    dict_data["GROUP"].append(summary.groups)
    dict_data["NETWORk"].append(summary.network)
  
  return dict_data 
 
