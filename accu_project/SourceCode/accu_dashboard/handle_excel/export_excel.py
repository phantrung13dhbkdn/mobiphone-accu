try:
    from BytesIO import BytesIO 
except ImportError:
    from io import BytesIO 

import pandas as pd
from __LP_Library.Microsoft_Office.Excel.draw_excel import DrawExcel
from __Config.Include.Common_Include import *
from accu_dashboard.handle_excel.get_data_excel import *

def draw_excel_summary(data_export):
  data_export = get_data_excel_summary(data_export)
  #3. Create a Pandas Excel writer using XlsxWriter as the engine.
  output = BytesIO() 
  writer = pd.ExcelWriter(output,engine='xlsxwriter')
  draw_excel_controller = DrawExcel(writer)

  #34) Seminar Property
  sheet_name = 'Summary'
  
  draw_excel_controller.draw_excel_table_only(sheet_name,data_export,pos_row=0)
  draw_excel_controller.set_resize_column(sheet_name,data_export, is_border=False)  


  writer.save()
  output.seek(0)

  response = HttpResponse(output, content_type='application/vnd.openxmlformats-officedocument.presentationml.presentation')

  file_name = 'Summary.xlsx'

  response['Content-Disposition'] = "attachment;filename={0}".format(file_name)
  
  return response

def draw_excel_summary_md(data_export):
  data_export = get_data_excel_summary_md(data_export)
  #3. Create a Pandas Excel writer using XlsxWriter as the engine.
  output = BytesIO() 
  writer = pd.ExcelWriter(output,engine='xlsxwriter')
  draw_excel_controller = DrawExcel(writer)

  #34) Seminar Property
  sheet_name = 'Summary MD'
  
  draw_excel_controller.draw_excel_table_only(sheet_name,data_export,pos_row=0)
  draw_excel_controller.set_resize_column(sheet_name,data_export, is_border=False)  


  writer.save()
  output.seek(0)

  response = HttpResponse(output, content_type='application/vnd.openxmlformats-officedocument.presentationml.presentation')

  file_name = 'Summary MD.xlsx'

  response['Content-Disposition'] = "attachment;filename={0}".format(file_name)
  
  return response

def draw_excel_summary_mpd(data_export):
  data_export = get_data_excel_summary_mpd(data_export)
  #3. Create a Pandas Excel writer using XlsxWriter as the engine.
  output = BytesIO() 
  writer = pd.ExcelWriter(output,engine='xlsxwriter')
  draw_excel_controller = DrawExcel(writer)

  #34) Seminar Property
  sheet_name = 'Summary MPD'
  
  draw_excel_controller.draw_excel_table_only(sheet_name,data_export,pos_row=0)
  draw_excel_controller.set_resize_column(sheet_name,data_export, is_border=False)  


  writer.save()
  output.seek(0)

  response = HttpResponse(output, content_type='application/vnd.openxmlformats-officedocument.presentationml.presentation')

  file_name = 'Summary MPD.xlsx'

  response['Content-Disposition'] = "attachment;filename={0}".format(file_name)
  
  return response

def draw_excel_summary_accu(data_export):
  data_export = get_data_excel_summary_accu(data_export)
  #3. Create a Pandas Excel writer using XlsxWriter as the engine.
  output = BytesIO() 
  writer = pd.ExcelWriter(output,engine='xlsxwriter')
  draw_excel_controller = DrawExcel(writer)

  #34) Seminar Property
  sheet_name = 'Summary ACCU'
  
  draw_excel_controller.draw_excel_table_only(sheet_name,data_export,pos_row=0)
  draw_excel_controller.set_resize_column(sheet_name,data_export, is_border=False)  


  writer.save()
  output.seek(0)

  response = HttpResponse(output, content_type='application/vnd.openxmlformats-officedocument.presentationml.presentation')

  file_name = 'Summary ACCU.xlsx'

  response['Content-Disposition'] = "attachment;filename={0}".format(file_name)
  
  return response  